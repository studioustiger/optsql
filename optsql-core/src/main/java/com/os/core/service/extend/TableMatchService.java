package com.os.core.service.extend;

import com.os.common.entity.extend.TableMatch;

import javax.servlet.http.HttpServletResponse;

/**
 * 描述：表匹配接口
 *
 * @author huxuehao
 **/
public interface TableMatchService {
    /* 表结构对比-下载文件 */
    void matchReturnFile(TableMatch tableMatch, HttpServletResponse response) throws Exception;

    /* 表结构对比-生成脚本 */
    String matchReturnScript(TableMatch tableMatch) throws Exception;
}
