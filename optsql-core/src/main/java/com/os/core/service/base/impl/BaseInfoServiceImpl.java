package com.os.core.service.base.impl;

import com.os.common.annotation.ds.DynamicDatasource;
import com.os.common.entity.table.ColumnDetails;
import com.os.common.entity.table.TableCreateSQL;
import com.os.common.entity.table.TableDesc;
import com.os.core.mapper.BaseInfoMapper;
import com.os.core.service.base.BaseInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 描述：基础实现实现类
 *
 * @author huxuehao
 **/
@Service
public class BaseInfoServiceImpl implements BaseInfoService {
    private final BaseInfoMapper baseInfoMapper;

    public BaseInfoServiceImpl(BaseInfoMapper baseInfoMapper) {
        this.baseInfoMapper = baseInfoMapper;
    }

    /**
     * 基于动态数据源获取表基础信息列表列表
     * @param datasourceId 数据源ID
     * @param tableName 表名
     * @param tableType 表类型
     */
    @Override
    @DynamicDatasource
    public List<TableDesc> selectList(String datasourceId, String tableName, String tableType) {
        return baseInfoMapper.selectList(tableName, tableType);
    }

    /**
     * 基于动态数据源获取表的列（字段）详情
     * @param datasourceId 数据源ID
     * @param tableName 表名
     */
    @Override
    @DynamicDatasource
    public List<ColumnDetails> selectColumnDetails(String datasourceId, String tableName){
        return baseInfoMapper.selectColumnDetails(tableName);
    }

    /**
     * 基于动态数据源获取表表创建语句
     * @param datasourceId 数据源ID
     * @param tableName 表名
     * @param tableType 表类型
     */
    @Override
    @DynamicDatasource
    public TableCreateSQL getCreateSql(String datasourceId, String tableName, String tableType) {
        return baseInfoMapper.getCreateSql(tableName, tableType);
    }
}
