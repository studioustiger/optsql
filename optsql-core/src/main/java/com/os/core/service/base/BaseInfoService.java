package com.os.core.service.base;

import com.os.common.entity.table.ColumnDetails;
import com.os.common.entity.table.TableCreateSQL;
import com.os.common.entity.table.TableDesc;

import java.util.List;

/**
 * 描述：基础信息接口
 *
 * @author huxuehao
 **/
public interface BaseInfoService {

    /* 基于动态数据源获取表基础信息列表 */
    List<TableDesc> selectList(String datasourceId, String tableName, String tableType) throws Exception;

    /* 基于动态数据源获取表的列（字段）详情 */
    List<ColumnDetails>selectColumnDetails(String datasourceId, String tableName) throws Exception;

    /* 基于动态数据源获取表表创建语句 */
    TableCreateSQL getCreateSql (String datasourceId, String tableName, String tableType) throws Exception;

}
