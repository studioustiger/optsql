package com.os.core.service.extend;

import com.os.common.entity.extend.DocumentInfo;

import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

/**
 * 描述：文档服务
 *
 * @author huxuehao
 **/
public interface DocumentService {
    /* work文档 */
    void genWord(DocumentInfo documentInfo, HttpServletResponse response) throws SQLException;

    /* html文档 */
    void genHtml(DocumentInfo documentInfo, HttpServletResponse response) throws SQLException;

    /* markdown文档 */
    void genMD(DocumentInfo documentInfo, HttpServletResponse response) throws SQLException;
}
