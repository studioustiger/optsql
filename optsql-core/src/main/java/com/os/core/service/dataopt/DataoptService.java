package com.os.core.service.dataopt;

import com.os.common.entity.extend.ParentChildMatrix;
import com.os.common.entity.field.FieldMerge;
import com.os.common.entity.field.FieldNameAlter;
import com.os.common.entity.foreignkey.RefTableNameAndColumnName;

import javax.naming.NotContextException;
import java.util.List;

/**
 * 描述：数据操作接口
 *
 * @author huxuehao
 **/
public interface DataoptService {

    /* 删除表数据 */
    List<String> deleteTableDataOfSimple(List<String> tableNames, String filterSql);

    /* 删除主键及其外键 */
    List<String> deleteTableDataOfPrimary(String datasourceId, List<String> tableNames, String filterSql);

    /* 修改字段值 */
    String alertFieldValue(FieldNameAlter fieldNameAlter);

    /* 合并字段*/
    String mergeFields(FieldMerge fieldMerge);

    /* 获取父子矩阵 */
    String parentChildMatrix(ParentChildMatrix matrix);

    /* 获取父子矩阵 */
    List<RefTableNameAndColumnName> foreignKeyOfPrimaryKey(String datasourceId, String tableName) throws NotContextException;
}
