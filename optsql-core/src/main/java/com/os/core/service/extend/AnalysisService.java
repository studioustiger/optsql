package com.os.core.service.extend;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * 描述：文件分析服务
 *
 * @author huxuehao
 **/
public interface AnalysisService {
    /* 解析Excel文件，生成脚本*/
    String analysisExcel(MultipartFile excelFile) throws Exception;
    /* 解析Csv文件，生成脚本*/
    String analysisCsv(MultipartFile csvFile) throws Exception;
    /* 解析Txt文件，生成脚本*/
    String analysisTxt(MultipartFile txtFile) throws Exception;

    /* 解析Excel文件，下载脚本*/
    void analysisExcelAndDownLoad(MultipartFile excelFile, HttpServletResponse response) throws Exception;
    /* 解析Csv文件，下载脚本*/
    void analysisCsvAndDownLoad(MultipartFile csvFile, HttpServletResponse response) throws Exception;
    /* 解析Txt文件，下载脚本*/
    void analysisTxtAndDownLoad(MultipartFile txtFile, HttpServletResponse response) throws Exception;
}
