package com.os.core.service.batch;

import com.os.common.entity.table.TableNameAlter;


/**
 * 描述：批量操作接口
 *
 * @author huxuehao
 **/
public interface BatchService {
    String AlterTableName(String datasourceId, TableNameAlter tableNameAlter) throws Exception;
}
