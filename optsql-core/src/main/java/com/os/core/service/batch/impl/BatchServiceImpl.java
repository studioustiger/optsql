package com.os.core.service.batch.impl;

import com.os.common.annotation.ds.DynamicDatasource;
import com.os.common.entity.table.TableNameAlter;
import com.os.common.exception.ErrorException;
import com.os.common.utils.MyUtil;
import com.os.core.service.batch.BatchService;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

/**
 * 描述：批量操作实现类
 *
 * @author huxuehao
 **/
@Service
public class BatchServiceImpl implements BatchService {
    /**
     * 修改表名
     */
    @Override
    @DynamicDatasource
    public String AlterTableName(String datasourceId, TableNameAlter tableNameAlter) {
        try {
            List<String> strings = new LinkedList<>();
            for (String originTableName : tableNameAlter.getOriginTableName()) {
                String[] split = originTableName.split(":");
                String oldTableName = split[0];
                String newTableName = null;
                String tableType = split[1];
                if ("insertPrefix".equalsIgnoreCase(tableNameAlter.getOptType())){
                    newTableName = insertPrefix(oldTableName, tableNameAlter.getNewSubName());
                } else if ("insertSuffix".equalsIgnoreCase(tableNameAlter.getOptType())) {
                    newTableName = insertSuffix(oldTableName, tableNameAlter.getNewSubName());
                } else if ("replaceAll".equalsIgnoreCase(tableNameAlter.getOptType())) {
                    newTableName = replaceEntire(oldTableName, tableNameAlter.getReplacedSubName(), tableNameAlter.getNewSubName());
                } else if ("replaceFirst".equalsIgnoreCase(tableNameAlter.getOptType())) {
                    newTableName = replaceHead(oldTableName, tableNameAlter.getReplacedSubName(), tableNameAlter.getNewSubName());
                } else if ("replaceLast".equalsIgnoreCase(tableNameAlter.getOptType())) {
                    newTableName = replaceFoot(oldTableName, tableNameAlter.getReplacedSubName(), tableNameAlter.getNewSubName());
                }
                if ("VIEW".equalsIgnoreCase(tableType)) {
                    strings.add("ALTER VIEW `"+oldTableName+"` RENAME `"+newTableName+"`;");
                } else {
                    strings.add("ALTER TABLE `"+oldTableName+"` RENAME `"+newTableName+"`;");
                }
            }
            return String.join("\n", strings);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ErrorException(e.getMessage());
        }
    }

    /**
     * 替换第一个符合条件的子串
     * @param originTableName originTableName 原表名
     * @param replacedSubName 被替换子串
     * @param newSubName  替换子串
     * @return 新表名
     */
    private String replaceHead(String originTableName, String replacedSubName, String newSubName) {

        return originTableName.replaceFirst(replacedSubName, newSubName);
    }

    /**
     * 替换最后一个符合条件的子串
     * @param originTableName originTableName 原表名
     * @param replacedSubName 被替换子串
     * @param newSubName 替换子串
     * @return 新表名
     */
    private String replaceFoot(String originTableName, String replacedSubName, String newSubName) {
        int lastAppearLocateIndex = MyUtil.subStrLastAppearLocate(originTableName,replacedSubName);

        if (lastAppearLocateIndex != -1) {
            return originTableName.substring(0, lastAppearLocateIndex)
                    + newSubName
                    + originTableName.substring(lastAppearLocateIndex + replacedSubName.length());
        }
        return originTableName;
    }

    /**
     * 替换全部服务条件的子串
     * @param originTableName originTableName 原表名
     * @param replacedSubName 被替换子串
     * @param newSubName 替换子串
     * @return 新表名
     */
    private String replaceEntire(String originTableName, String replacedSubName, String newSubName) {

        return originTableName.replaceAll(replacedSubName, newSubName);
    }

    /**
     * 插入后缀
     * @param originTableName originTableName 原表名
     * @param newSubName 待插入后缀值
     * @return 新表名
     */
    private String insertSuffix(String originTableName, String newSubName) {

        return originTableName + newSubName;
    }

    /**
     * 插入前缀
     * @param originTableName 原表名
     * @param newSubName 待插入后缀值
     * @return 新表名
     */
    private String insertPrefix(String originTableName, String newSubName) {

        return newSubName + originTableName;
    }
}
