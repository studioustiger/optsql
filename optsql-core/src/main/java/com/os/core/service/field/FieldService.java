package com.os.core.service.field;


import com.os.common.entity.field.FieldAdd;
import com.os.common.entity.field.FieldAlter;
import com.os.common.entity.field.FieldDelete;

import java.util.List;

/**
 * 描述：字段服务接口
 *
 * @author huxuehao
 **/
public interface FieldService {
    List<String> filedBatchAdd(String datasourceId, FieldAdd fieldAdd);
    List<String> filedBatchDelete(String datasourceId, FieldDelete fieldAdd);
    List<String> filedBatchAlter(String datasourceId, FieldAlter fieldAdd);
}