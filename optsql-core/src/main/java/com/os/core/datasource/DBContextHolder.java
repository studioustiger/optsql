package com.os.core.datasource;

import com.os.common.utils.MyUtil;
import org.springframework.stereotype.Component;

/**
 * 描述：缓存数据源(id）
 *
 * @author huxuehao
 **/
@Component
public class DBContextHolder {
    // 对当前线程的操作-线程安全的
    private static final ThreadLocal<String> CONTEXT_HOLDER = new ThreadLocal<>();
 
    // 调用此方法，切换数据源
    public static void setDataSource(String dataSource) {
        CONTEXT_HOLDER.set(dataSource);
    }
 
    // 获取数据源
    public static String getDataSource() {
        return CONTEXT_HOLDER.get();
    }
 
    // 删除数据源
    public static void clearDataSource() {
        String dataSource = getDataSource();
        if (!MyUtil.isEmpty(dataSource)) {
            CONTEXT_HOLDER.remove();
        }
    }
}