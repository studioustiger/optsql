package com.os.core.datasource;

import com.os.common.entity.datasource.DataSource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 描述：数据源配置缓存
 *
 * @author huxuehao
 **/
public class DataSourceConfigCache {
    /* 缓存 */
    private static final Map<String, DataSource> configMap = new ConcurrentHashMap<>(16);

    /* 设置缓存 */
    public static void put(DataSource dataSource) {
        putAll(Collections.singletonList(dataSource));
    }

    /* 设置缓存 */
    public static void putAll(List<DataSource> dataSources) {
        dataSources.forEach(v0 -> configMap.put(v0.getId(), v0));
    }

    /* 获取指定缓存 */
    public static DataSource get(String dataSourceId) {

        return configMap.get(dataSourceId);
    }

    /* 获取所有缓存 */
    public static List<DataSource> getAll() {
        ArrayList<DataSource> DataSources = new ArrayList<>();
        configMap.forEach((key, value) -> DataSources.add(value));
        return DataSources;
    }

    /* 删除指定缓存 */
    public static DataSource delete(String dataSourceId) {

        return configMap.remove(dataSourceId);
    }

    /* 删除所有缓存 */
    public static void deleteAll() {
        configMap.clear();
    }

    /* 判断是否存在 */
    public static boolean exist(String dataSourceId) {
        return configMap.containsKey(dataSourceId);
    }

    /* 判断是否不存在 */
    public static boolean notExist(String dataSourceId) {
        return !exist(dataSourceId);
    }
}
