package com.os.core.mapper;

import com.os.common.entity.extend.ColumnInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.LinkedList;

/**
 * 描述：扩展模块数据库操作
 *
 * @author huxuehao
 **/
@Repository
public interface ExtendMapper {

    LinkedList<String> getTableNames();

    LinkedList<ColumnInfo> getColumnInfo(@Param("tableName") String tableName);
}
