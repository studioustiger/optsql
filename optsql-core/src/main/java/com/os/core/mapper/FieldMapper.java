package com.os.core.mapper;

import com.os.common.entity.field.FieldAlter;
import com.os.common.entity.field.FieldDelete;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.LinkedList;
import java.util.Map;

/**
 * 描述：字段模块数据库操作
 *
 * @author huxuehao
 **/
@Repository
public interface FieldMapper {
    LinkedList<String> regexField(@Param("fieldDelete") FieldDelete fieldDelete);
    @MapKey("COLUMN_NAME")
    LinkedList<Map<String,String>> regexMatch(@Param("fieldAlter") FieldAlter fieldAlter);
}
