package com.os.core.mapper;

import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 描述：数据操作模块数据库操作
 *
 * @author huxuehao
 **/
@Repository
public interface DataoptMapper {
    @MapKey("TABLE_NAME")
    LinkedList<Map<String,String>> containView(@Param("tableNames") List<String> tableNames);

    @MapKey("SOURCE")
    LinkedList<Map<String,String>> getForeignKeyInfo(@Param("tableNames") List<String> tableNames);

    @MapKey("TABLE_NAME")
    LinkedList<Map<String,String>> getPrimary(@Param("tableNames") List<String> tableNames);

    @MapKey("OPERATION_SQL_END")
    LinkedList<Map<String,String>> foreignKeyOfPrimaryKey(@Param("targetTableName") String targetTableName,
                                                    @Param("targetColumns") List<String> targetColumns,
                                                    @Param("primaryTableName") String primaryTableName,
                                                    @Param("primaryColumn") String primaryColumn);

    LinkedList<String> getTableNames();
    LinkedList<String> getColumns(@Param("tableName") String tableName);
}
