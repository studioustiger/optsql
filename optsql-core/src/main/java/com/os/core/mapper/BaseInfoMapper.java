package com.os.core.mapper;

import com.os.common.entity.table.ColumnDetails;
import com.os.common.entity.table.TableCreateSQL;
import com.os.common.entity.table.TableDesc;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.LinkedList;

/**
 * 描述：基础信息模块数据库操作
 *
 * @author huxuehao
 **/
@Repository
public interface BaseInfoMapper {
    LinkedList<TableDesc> selectList(@Param("tableName") String tableName, @Param("tableType")String tableType);

    LinkedList<ColumnDetails> selectColumnDetails(@Param("tableName") String tableName);

    TableCreateSQL getCreateSql(@Param("tableName") String tableName, @Param("tableType") String tableType);
}
