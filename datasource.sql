
--数据源表
CREATE TABLE `os_datasource`  (
  `id` varchar(20) NOT NULL COMMENT '主键',
  `name` varchar(255) NULL DEFAULT NULL COMMENT '数据源名称',
  `drive` varchar(100) NULL DEFAULT NULL COMMENT '驱动',
  `url` varchar(500) NULL DEFAULT NULL COMMENT '连接信息',
  `user_name` varchar(255) NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) NULL DEFAULT NULL COMMENT '密码',
  `database_type` varchar(255) NULL DEFAULT NULL COMMENT '数据库类型',
  `code` varchar(255) NULL DEFAULT NULL COMMENT '暂留字段',
  `del_flag` int(0) NULL DEFAULT 0 COMMENT '是否删除（1是 0否）',
  PRIMARY KEY (`id`) USING BTREE
) COMMENT = '数据源';

-- 菜单表
CREATE TABLE `os_menu`  (
  `id` varchar(20) NOT NULL COMMENT '主键',
  `parent_id` bigint(0) NULL DEFAULT 0 COMMENT '父级菜单',
  `name` varchar(255) NULL DEFAULT NULL COMMENT '菜单名称',
  `alias` varchar(255) NULL DEFAULT NULL COMMENT '菜单别名',
  `path` varchar(1000) NULL DEFAULT NULL COMMENT '请求地址',
  `component` varchar(255) NULL DEFAULT NULL COMMENT '组件路径',
  `params` varchar(500) NULL DEFAULT NULL COMMENT '路由参数',
  `icon` varchar(100) NULL DEFAULT '#' COMMENT '菜单图标',
  `is_frame` int(0) NULL DEFAULT 0 COMMENT '是否为外链（1是 0否）',
  `is_open` int(0) NULL DEFAULT 0 COMMENT '是否打开新页面（1是 0否）',
  `sort` int(0) NULL DEFAULT NULL COMMENT '排序',
  `del_flag` int(0) NULL DEFAULT 0 COMMENT '是否删除（1是 0否）',
  PRIMARY KEY (`id`) USING BTREE
) COMMENT = '菜单表';


-- 设置配置表
CREATE TABLE `os_setting`  (
  `id` varchar(20) NOT NULL COMMENT '主键',
  `code` varchar(255) NULL DEFAULT NULL COMMENT '编号',
  `name` varchar(255) NULL DEFAULT NULL COMMENT '设置名称',
  `enable` int(0) NULL DEFAULT NULL COMMENT '是否禁用（1表示可用，0表示禁用）',
  `del_flag` varchar(255) NULL DEFAULT NULL COMMENT '是否删除（0表示不删除，1表示删除）',
  PRIMARY KEY (`id`) USING BTREE
) COMMENT = '设置配置';
INSERT INTO `os_setting` VALUES ('123456', 'search', '搜索', 1, '0');
INSERT INTO `os_setting` VALUES ('123457', 'lock', '锁屏', 1, '0');
INSERT INTO `os_setting` VALUES ('123458', 'full', '全屏', 1, '0');
INSERT INTO `os_setting` VALUES ('123459', 'theme', '暗系', 1, '0');


-- 用户信息表
CREATE TABLE `os_user`  (
  `id` varchar(20) NOT NULL COMMENT '主键',
  `user_name` varchar(255) NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) NULL DEFAULT NULL COMMENT '密码',
  `del_flag` int(0) NULL DEFAULT 0 COMMENT '是否删除（1是 0否）',
  PRIMARY KEY (`id`) USING BTREE
) COMMENT = '用户信息表';
-- 用户名和密码相同
INSERT INTO `os_user` VALUES ('1111111111111111111', 'os-admin', 'e32d7dc9989722834862f517244a7c5a', 0);
