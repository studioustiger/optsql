/**
 * 全站http配置
 *
 */

import axios from 'axios'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import { getToken } from '@/utils/auth'
import { tansParams } from "@/utils/util";
import website from '@/config/website'
import cache from '@/plugins/cache'
import router from '@/router'

// 连续操作白名单
const whiteList = ['/datasource/selectList']

// 创建axios实例
const service = axios.create({
    // 环境变量base接口地址 url = base url + request url
    baseURL: website.mode === 'web' ? process.env.VUE_APP_BASE_API : '',
    // 跨域请求时发送Cookie
    withCredentials: false,
    // 请求超时
    timeout: 60000,
    // 设置默认请求头
    headers: {
      "Content-Type": "application/json; charset=UTF-8;"
    },
    //返回其他状态码
    validateStatus: (status) => {
        return status >= 200 && status <= 500
    }
});

// NProgress 配置
NProgress.configure({
    easing:'ease',
    speed:150,
    showSpinner:false
  });

//http request拦截
service.interceptors.request.use(config => {
    //开启 progress bar
    NProgress.start()
    config.headers.Cookie = null

    // 判断是否需要设置token
    const isToken = (config.headers || {}).isToken === false
    if (getToken() && !isToken) {
        // 配置token
        config.headers[website.tokenHeader] = getToken()
    }

    // get 请求映射 params 参数
    if (config.method === 'get' && config.params) {
        let url = config.url + "?" + tansParams(config.params)
        url = url.slice(0, -1)
        config.params = {}
        config.url = url
    }

    // 防止连续操作
    if (website.isRepeatSubmit && whiteList.indexOf(config.url) === -1) {
        const requestObj = {
            url: config.url,
            data: typeof config.data === 'object' ? JSON.stringify(config.data) : config.data,
            time: new Date().getTime()
        }
        // 从session中获取请求体，并判断是否为第一次请求
        const sessionObj = cache.session.getJSON('sessionObj')
        if (sessionObj === undefined || sessionObj === null || sessionObj === '') {
            cache.session.setJSON('sessionObj', requestObj)
        }
        // 判断在指定时间内是连续操作
        else {
            const l_url = sessionObj.url;     // 上次的请求地址
            const l_data = sessionObj.data;   // 上次的请求数据
            const l_time = sessionObj.time;   // 上次的请求时间
            const interval = 500;            // 间隔时间(ms)，小于此时间视为重复提交
            if (l_data === requestObj.data && requestObj.time - l_time < interval && l_url === requestObj.url) {
                cache.session.setJSON('sessionObj', requestObj)
                return Promise.reject('请勿连续操作')
            } else {
                cache.session.setJSON('sessionObj', requestObj)
            }
        }
    }
    return config
}, error => {
    return Promise.reject(error)
})

//http response 拦截
service.interceptors.response.use(res => {
    // 二进制数据直接返回
    if (res.request.responseType === 'blob' || res.request.responseType === 'arraybuffer') {
    // if (res.headers.download === 'true') {
        NProgress.done()
        return res
    }
    // 请求失败
    if(res.data.code !== 200) {
        const code = res.data.code || 500
        const msg = res.data.msg || '未知错误'
        if (code === 401) {
            // 登录验证失败，跳转到登录界面
            router.push({path:'/login'})
        }
        NProgress.done()
        return Promise.reject(msg)
    }
    NProgress.done()
    return res
}, error => {
    NProgress.done()
    return Promise.reject(error)
})

export default service
