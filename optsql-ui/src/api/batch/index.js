/**
 * 表名批处理
 */

import request from '@/api/axios'

/**
 * @description xxxxx
 * @param {*} param1 
 * @param {*} param2 
 * @returns 
 */
export const xxx = (param1,param2) => {
    return request({
        url: '/xxx/xxx/xxx',
        method: 'get',
        params: {
            param1,
            param2
          }
    })
}

/**
 * @description 批量修改表名
 * @param {*} obj 
 * @returns 
 */
export const getAlterTableNameSql = (obj) => {
    return request({
        url: '/batch/alter/tableName/sql',
        method: 'post',
        data: obj
    })
}