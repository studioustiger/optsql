/**
 * 设置
 */

import request from '@/api/axios'

/**
 * @description 修改密码
 * @param {*} obj 
 * @returns 
 */
export const changePassword = (obj) => {
    return request({
        url: '/setting/change/password',
        method: 'post',
        data: obj
    })
}

/**
 * @description 获取setting列表
 * @returns 
 */
export const selectSettingList = () => {
    return request({
        url: '/setting/select/setting',
        method: 'get',
    })
}

/**
 * @description 修改设置状态
 * @param {*} obj 
 * @returns 
 */
export const changeSettingStatus = (obj) => {
    return request({
        url: '/setting/change/setting',
        method: 'post',
        data: obj
    })
}