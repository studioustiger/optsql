/**
 * 动态数据源
 */

import request from '@/api/axios'

/**
 * @description 获取数据源列表
 * @param {*} param1 
 * @param {*} param2 
 * @returns 
 */
export const selectDataSourceList = () => {
    return request({
        url: '/datasource/selectList',
        method: 'get'
    })
}

/**
 * @description 新增数据源
 * @param {*} obj 
 * @returns 
 */
export const addDataSource = (obj) => {
    return request({
        url: '/datasource/add',
        method: 'post',
        data: obj
    })
}
/**
 * @description 删除数据源
 * @param {*} obj 
 * @returns 
 */
export const deleteDataSource = (obj) => {
    return request({
        url: '/datasource/deleteByIds',
        method: 'post',
        data: obj
    })
}

/**
 * @description 更新数据源
 * @param {*} obj 
 * @returns 
 */
export const updateDataSource = (obj) => {
    return request({
        url: '/datasource/update',
        method: 'post',
        data: obj
    })
}