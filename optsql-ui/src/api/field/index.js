/**
 * 表字段操作
 */

import request from '@/api/axios'

/**
 * @description xxx
 * @param {*} param1 
 * @param {*} param2 
 * @returns 
 */
export const xxx = (param1,param2) => {
    return request({
        url: '/xxx/xxx/xxx',
        method: 'get',
        params: {
            param1,
            param2
        }
    })
}

/**
 * @description 添加字段
 * @param {*} obj 
 * @returns 
 */
export const addField = (obj) => {
    return request({
        url: '/filed/batch/add',
        method: 'post',
        data: obj
    })
}

/**
 * @description 删除字段
 * @param {*} obj 
 * @returns 
 */
export const deleteField = (obj) => {
    return request({
        url: '/filed/batch/delete',
        method: 'post',
        data: obj
    })
}

/**
 * @description 修改字段
 * @param {*} obj 
 * @returns 
 */
export const alterField = (obj) => {
    return request({
        url: '/filed/batch/alter',
        method: 'post',
        data: obj
    })
}