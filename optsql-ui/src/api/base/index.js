/**
 * 表基础信息
 */

import request from '@/api/axios'

/**
 * @description 获取数据库表基本信息
 * @param {*} datasourceId 数据源ID
 * @param {*} tableName    表名
 * @param {*} tableType    表类型
 * @returns 
 */
export const selectBaseInfoList = (datasourceId, tableName, tableType) => {
    return request({
        url: '/base/info/select',
        method: 'get',
        params: {
            datasourceId,
            tableName,
            tableType
        }
    })
}

/**
 * @description 获取数据库表的列（字段详情）
 * @param {*} datasourceId 数据源ID
 * @param {*} tableName    表名
 * @param {*} tableType    表类型
 * @returns 
 */
export const selectColumnDetails = (datasourceId, tableName) => {
    return request({
        url: '/base/info/column-details',
        method: 'get',
        params: {
            datasourceId,
            tableName
        }
    })
}

/**
 * @description 获取DDL
 * @param {*} datasourceId 数据源ID
 * @param {*} tableName    表名
 * @param {*} tableType    表类型
 * @returns 
 */
export const getDDL = (datasourceId, tableName, tableType) => {
    return request({
        url: '/base/info/ddl',
        method: 'get',
        params: {
            datasourceId,
            tableName,
            tableType
        }
    })
}