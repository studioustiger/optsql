/**
 * 文件解析获取脚本、文件解析下载脚本
 */

import request from '@/api/axios'

/**
 * @description 解析文件，获取导入脚本
 * @param {*} file 
 * @returns 
 */
export const analysisFile = (file) => {
    return request({
        headers: {
            'Content-Type': 'multipart/form-data'
        },
        url: '/extend/file/analysis/file',
        method: 'post',
        data: file
    })
}

/**
 * @description 解析excel，获取导入脚本
 * @param {*} excelFile 
 * @returns 
 */
export const analysisExcel = (excelFile) => {
    return request({
        headers: {
            'Content-Type': 'multipart/form-data'
        },
        url: '/extend/file/analysis/excel',
        method: 'post',
        data: excelFile
    })
}

/**
 * @description 解析scv，获取导入脚本
 * @param {*} csvFile 
 * @returns 
 */
export const analysisCsv = (csvFile) => {
    return request({
        headers: {
            'Content-Type': 'multipart/form-data'
        },
        url: '/extend/file/analysis/scv',
        method: 'post',
        params: {
            csvFile
        }
    })
}

/**
 * @description 解析txt，获取导入脚本
 * @param {*} txtFile 
 * @returns 
 */
export const analysisTxt = (txtFile) => {
    return request({
        headers: {
            'Content-Type': 'multipart/form-data'
        },
        url: '/extend/file/analysis/txt',
        method: 'post',
        params: {
            txtFile
        }
    })
}

/**
 * @description 解析文件，下载导入脚本
 * @param {*} file 
 * @returns 
 */
export const analysisFileAndDownload = (file) => {
    return request({
        headers: {
            'Content-Type': 'multipart/form-data'
        },
        responseType: 'blob',
        url: '/extend/file/analysis/file/download',
        method: 'post',
        data: file
    })
}

/**
 * @description 解析excel文件，下载导入脚本
 * @param {*} file 
 * @returns 
 */
export const analysisExcelAndDownload = (file) => {
    return request({
        headers: {
            'Content-Type': 'multipart/form-data'
        },
        responseType: 'blob',
        url: '/extend/file/analysis/excel/download',
        method: 'post',
        data: file
    })
}

/**
 * @description 解析csv文件，下载导入脚本
 * @param {*} file 
 * @returns 
 */
export const analysisCsvAndDownload = (file) => {
    return request({
        headers: {
            'Content-Type': 'multipart/form-data'
        },
        responseType: 'blob',
        url: '/extend/file/analysis/csv/download',
        method: 'post',
        data: file
    })
}

/**
 * @description 解析txt文件，下载导入脚本
 * @param {*} file 
 * @returns 
 */
export const analysisTxtAndDownload = (file) => {
    return request({
        headers: {
            'Content-Type': 'multipart/form-data'
        },
        responseType: 'blob',
        url: '/extend/file/analysis/txt/download',
        method: 'post',
        data: file
    })
}

/**
 * @description xxxxx
 * @param {*} obj 
 * @returns 
 */
export const xxx2 = (obj) => {
    return request({
        url: '/xxx2/xxx2/xxx2',
        method: 'post',
        data: obj
    })
}