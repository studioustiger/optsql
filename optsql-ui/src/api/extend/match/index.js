/**
 * 表名的差异
 */

import request from '@/api/axios'

/**
 * @description 表结构对比-生成脚本
 * @param {*} obj 
 * @returns 
 */
export const matchReturnScript = (obj) => {
    return request({
        url: '/extend/table/match/script',
        method: 'post',
        data: obj
    })
}

/**
 * @description 表结构对比-下载文件
 * @param {*} obj 
 * @returns 
 */
export const matchReturnFile = (obj) => {
    return request({
        responseType: 'blob',
        url: '/extend/table/match/file',
        method: 'post',
        data: obj
    })
}

/**
 * @description xxxxx
 * @param {*} param1 
 * @param {*} param2 
 * @returns 
 */
export const xxx = (param1,param2) => {
    return request({
        url: '/xxx/xxx/xxx',
        method: 'get',
        params: {
            param1,
            param2
          }
    })
}