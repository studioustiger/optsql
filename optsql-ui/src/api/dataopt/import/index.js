/**
 * 文件导入
 */

import request from '@/api/axios'

/**
 * @description xxxxx
 * @param {*} param1 
 * @param {*} param2 
 * @returns 
 */
export const xxx = (param1,param2) => {
    return request({
        url: '/xxx/xxx/xxx',
        method: 'get',
        params: {
            param1,
            param2
          }
    })
}

/**
 * @description xxxxx
 * @param {*} obj 
 * @returns 
 */
export const xxx2 = (obj) => {
    return request({
        url: '/xxx2/xxx2/xxx2',
        method: 'post',
        data: obj
    })
}