/**
 * 删除数据
 */

import request from '@/api/axios'

/**
 * @description 简单删除表数据
 * @param {*} obj
 * @returns 
 */
export const deleteTableDataOfSimple = (obj) => {
    return request({
        url: '/dataopt/delete/table-data/simple',
        method: 'post',
        data: obj
    })
}

/**
 * @description 删除主键及其外键数据
 * @param {*} obj 
 * @returns 
 */
export const deleteTableDataOfPrimary = (obj) => {
    return request({
        url: '/dataopt/delete/table-data/primary',
        method: 'post',
        data: obj
    })
}
