/**
 * 查询数据
 */

import request from '@/api/axios'

/**
 * @description 获取表的逻辑外键
 * @param {*} tableName 表名 
 * @returns 
 */
export const foreignKeyOfPrimaryKey = (datasourceId,tableName) => {
    return request({
        url: '/dataopt/foreignKey/of/primaryKey',
        method: 'get',
        params: {
            datasourceId,
            tableName
        }
    })
}

/**
 * @description 获取父子矩阵
 * @param {*} obj 
 * @returns 
 */
export const parentChildMatrix = (obj) => {
    return request({
        url: '/dataopt/matrix/parent-child',
        method: 'post',
        data: obj
    })
}