/**
 * 修改表数据
 */

import request from '@/api/axios'

/**
 * @description 给指定字段添加前缀或后缀
 * @param {*} obj 
 * @returns 
 */
export const alertFieldValue = (obj) => {
    return request({
        url: '/dataopt/alert/field-value',
        method: 'post',
        data: obj
    })
}

/**
 * @description 合并字段
 * @param {*} obj 
 * @returns 
 */
export const mergeFields = (obj) => {
    return request({
        url: '/dataopt/merge/fields',
        method: 'post',
        data: obj
    })
}

/**
 * @description xxxxx
 * @param {*} param1 
 * @param {*} param2 
 * @returns 
 */
export const xxx = (param1,param2) => {
    return request({
        url: '/xxx/xxx/xxx',
        method: 'get',
        params: {
            param1,
            param2
        }
    })
}

/**
 * @description xxxxx
 * @param {*} obj 
 * @returns 
 */
export const xxx2 = (obj) => {
    return request({
        url: '/xxx2/xxx2/xxx2',
        method: 'post',
        data: obj
    })
}