/**
 * 鉴权
 */

import request from '@/api/axios'

/**
 * @description 退出
 * @param {*} userId
 * @returns 
 */
export const logout = (userId) => {
    return request({
        url: '/logout',
        method: 'get',
        params: {   
            userId
        }
    })
}

/**
 * @description 登录
 * @param {*} obj 
 * @returns 
 */
export const login = (obj) => {
    return request({
        url: '/login',
        method: 'post',
        data: obj
    })
}