/**
 * 网站配置文件
 */
const obj = {
    version: '1.0',
    copyright: '2023 studioustiger all rights reserved.',
    indexTitle: '数据库批处理工具 about mysql',
    tokenHeader: 'OptSql-Auth',
    /* 是否开启防止重复提交 */
    isRepeatSubmit: true,
    mode: 'boot', // boot 模式为前端端不分离，web 模式为前后端分离
}
// 兼容ES6语法，在vue.config.js中使用
module.exports = obj
