export default
    {
        sideNav: [
            {
                icon: "dynamic-datasource",
                name: "动态数据源",
                path: "/dynamic/datasource",
                component: "@/views/datasource",
                id: '12345'
            },
            {
                icon: "table-baseinfo",
                name: "表基础信息",
                path: "/base/info",
                component: "@/views/base",
                id: '22345'
            },
            {
                icon: "tablename-batch",
                name: "表名批处理",
                path: "/tablename/batch",
                component: "@/views/batch",
                id: '32345'
            },
            {
                icon: "tablefield-alter",
                name: "表字段修改",
                path: "/field/alter",
                component: "@/views/field",
                id: '42345'
            },
            {
                icon: "data-opt",
                name: "表数据变更",
                path: "/data/opt",
                component: "@/views/dataopt",
                id: '52345'
            },
            // {
            //     icon: "database-transfer",
            //     name: "数据库迁移",
            //     path: "/data/transfer",
            //     component: "@/views/transfer",
            //     id: '62345'
            // },
            {
                icon: "extend-utils",
                name: "扩展工具集",
                path: "/extend/utils",
                component: "@/views/extend",
                id: '72345'
            }
        ],
        subMenu: [
            {
                parentId: '12345',
                menuList: [
                    {
                        id: '123456a',
                        icon: 'sub-menu-icon',
                        name: '创建动态数据源',
                        path: "/dynamic/datasource/create",
                        component: "@/views/datasource/create",
                    }
                ]
            },
            {
                parentId: '22345',
                menuList: [
                    {
                        id: '223456a',
                        icon: 'sub-menu-icon',
                        name: '基本信息',
                        path: "/base/info/index",
                        component: "@/views/base/index",
                    },
                    // {
                    //     id: '223456b',
                    //     icon: 'sub-menu-icon',
                    //     name: 'DDL操作',
                    //     path: "/system/base/info/ddl",
                    //     component: "@/views/base/ddl",
                    // }
                ]
            },
            {
                parentId: '32345',
                menuList: [
                    {
                        id: '323456a',
                        icon: 'sub-menu-icon',
                        name: '批量修改表名',
                        path: "/tablename/batch/alter",
                        component: "@/views/batch/alter",
                    }
                ]
            },
            {
                parentId: '42345',
                menuList: [
                    {
                        id: '423456a',
                        icon: 'sub-menu-icon',
                        name: '批量新增表字段',
                        path: "/field/alter/add",
                        component: "@/views/field/add",
                    },
                    {
                        id: '423456b',
                        icon: 'sub-menu-icon',
                        name: '批量删除表字段',
                        path: "/field/alter/delete",
                        component: "@/views/field/delete",
                    },
                    {
                        id: '423456c',
                        icon: 'sub-menu-icon',
                        name: '批量修改表字段',
                        path: "/field/alter/update",
                        component: "@/views/field/update",
                    }
                ]
            },
            {
                parentId: '52345',
                menuList: [
                    {
                        id: '523456a',
                        icon: 'sub-menu-icon',
                        name: '删除表数据',
                        path: "/data/opt/delete",
                        component: "@/views/dataopt/delete",
                    },
                    {
                        id: '523456b',
                        icon: 'sub-menu-icon',
                        name: '修改表数据',
                        path: "/data/opt/alter",
                        component: "@/views/dataopt/alter",
                    },
                    {
                        id: '523456c',
                        icon: 'sub-menu-icon',
                        name: '查询表数据',
                        path: "/data/opt/query",
                        component: "@/views/dataopt/query",
                    },
                    // {
                    //     id: '523456d',
                    //     icon: 'sub-menu-icon',
                    //     name: '文件导入库',
                    //     path: "/data/opt/import",
                    //     component: "@/views/dataopt/import",
                    // }
                ]
            },
            // {
            //     parentId: '62345',
            //     menuList: [
            //         {
            //             id: '623456a',
            //             icon: 'sub-menu-icon',
            //             name: '简单数据迁移',
            //             path: "/data/transfer/simple",
            //             component: "@/views/transfer/simple",
            //         },
            //         {
            //             id: '623456d',
            //             icon: 'sub-menu-icon',
            //             name: 'ID类型转换迁移',
            //             path: "data/transfer/switchId",
            //             component: "@/views/transfer/switchId",
            //         }
            //     ]
            // },
            {
                parentId: '72345',
                menuList: [
                    {
                        id: '723456a',
                        icon: 'sub-menu-icon',
                        name: '模板SQL器',
                        path: "/extend/utils/sqltemple",
                        component: "@/views/extend/sqltemple",
                    },
                    {
                        id: '723456b',
                        icon: 'sub-menu-icon',
                        name: '表结构同步',
                        path: "/extend/utils/differ",
                        component: "@/views/extend/differ",
                    },
                    {
                        id: '723456d',
                        icon: 'sub-menu-icon',
                        name: '解析文件表',
                        path: "/extend/utils/file/analysis",
                        component: "@/views/extend/analysis",
                    },
                    {
                        id: '723456c',
                        icon: 'sub-menu-icon',
                        name: '数据库文档',
                        path: "/extend/utils/dev-doc",
                        component: "@/views/extend/dev-doc",
                    }
                ]
            }
        ],
        menuTree: [
            {
                name: "动态数据源",
                path: "/dynamic/datasource",
                children: [
                    {
                        name: '创建动态数据源',
                        path: "/dynamic/datasource/create",
                    }
                ]
            },
            {
                name: "表基础信息",
                path: "/base/info",
                children: [
                    {
                        name: '基本信息',
                        path: "/base/info/index",
                    },
                    // {
                    //     name: 'DDL操作',
                    //     path: "/base/info/ddl",
                    // }
                ]
            }, {
                name: "表名批处理",
                path: "/tablename/batch",
                children: [
                    {
                        name: '批量修改表名',
                        path: "/tablename/batch/alter",
                    }
                ]
            },
            {
                name: "表字段修改",
                path: "/field/alter",
                children: [
                    {
                        name: '批量新增表字段',
                        path: "/field/alter/add",
                    },
                    {
                        name: '批量删除表字段',
                        path: "/field/alter/delete",
                    },
                    {
                        name: '批量修改表字段',
                        path: "/field/alter/update",
                    }
                ]
            },
            {
                name: "表数据变更",
                path: "/data/opt",
                children: [
                    {
                        name: '删除表数据',
                        path: "/data/opt/delete",
                    },
                    {
                        name: '修改表数据',
                        path: "/data/opt/alter",
                    },
                    {
                        name: '查询表数据',
                        path: "/data/opt/query",
                    },
                    // {
                    //     name: '文件导入库',
                    //     path: "/data/opt/import",
                    // }
                ]
            },
            // {
            //     name: "数据库迁移",
            //     path: "/data/transfer",
            //     children: [
            //         {
            //             name: '简单数据迁移',
            //             path: "/data/transfer/simple",
            //         },
            //         {
            //             name: 'ID类型转换迁移',
            //             path: "/data/transfer/switchId",
            //         }
            //     ]
            // },
            {
                name: "扩展工具集",
                path: "/extend/utils",
                children: [
                    {
                        name: '模板SQL器',
                        path: "/extend/utils/sqltemple",
                    },
                    {
                        name: '表结构同步',
                        path: "/extend/utils/differ",
                    },
                    {
                        name: '解析文件表',
                        path: "/extend/utils/file/analysis",
                    },
                    {
                        name: '数据库文档',
                        path: "/extend/database/dev-doc",
                    },
                ]

            }
        ],
    }