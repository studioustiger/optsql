import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import views from './views'
import website from '@/config/website'

/* 防止连续点击多次路由报错 */
let routerPush = Router.prototype.push;
Router.prototype.push = function push(location) {
  return routerPush.call(this, location).catch(err => err)
}

export default new Router(
  {
    mode: website.mode === 'web' ? 'history' : 'hash',
    base: website.mode === 'web' ? window.__MICRO_APP_BASE_ROUTE__ || process.env.VUE_APP_BASE_URL : '',
    routes: [
      {
        path: '/',
        component: () => import('@/layout'),
        redirect: 'index',
        children: [
          ...views,
        ]
        // children: [
        //   {
        //     path: '/system',
        //     component: () => import('@/views'),
        //     redirect: 'system/index',
        //     children: [
        //       ...views,
        //     ]
        //   }
        // ]

      },
      {
        path: '/login',
        component: () => import('@/views/login'),
        meta:
        {
          title: '登录'
        }
      },
      {
        path: process.env.VUE_APP_LOCK_SCREEN_PATH,
        component: () => import('@/layout/components/TopToolbar/lockScreen'),
        meta:
        {
          title: '锁屏'
        }
      },
      {
        path: '*',
        component: () => import('@/views/404'),
        meta:
        {
          title: 'Not Found',
        }
      },
    ]
  }
)
