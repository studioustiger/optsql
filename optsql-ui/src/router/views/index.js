export default [
    {
        path: 'index',
        name: "首页",
        redirect: 'dynamic/datasource',
    },
    {
        path: 'dynamic/datasource',
        name: "动态数据源",
        component: () => import('@/views'),
        redirect: 'dynamic/datasource/create',
        children: [
            {
                path: 'create',
                name: '创建动态数据源',
                component: () => import("@/views/datasource/create"),
                meta:
                {
                    title: '创建动态数据源',
                }
            }
        ]
    },
    {
        path: 'base/info',
        name: '表基础信息',
        component: () => import('@/views'),
        redirect: 'base/info/index',
        children: [
            {
                path: 'index',
                name: '基本信息',
                component: () => import("@/views/base/info"),
                meta:
                {
                    title: '基本信息',
                }
            },
            {
                path: 'ddl',
                name: 'DDL操作',
                component: () => import("@/views/base/ddl"),
                meta:
                {
                    title: 'DDL操作',
                }
            },
        ]
    },
    {
        path: 'tablename/batch',
        name: '表名批处理',
        component: () => import('@/views'),
        redirect: 'tablename/batch/alter',
        children: [
            {
                path: 'alter',
                name: '批量修改表名',
                component: () => import("@/views/batch/alter"),
                meta:
                {
                    title: '批量修改表名',
                }
            }
        ]
    },
    {
        path: 'field/alter',
        name: '表字段修改',
        component: () => import('@/views'),
        redirect: 'field/alter/add',
        children: [
            {
                path: 'add',
                name: '批量新增表字段',
                component: () => import("@/views/field/add"),
                meta:
                {
                    title: '批量新增表字段',
                }
            },
            {
                path: 'delete',
                name: '批量删除表字段',
                component: () => import("@/views/field/delete"),
                meta:
                {
                    title: '批量删除表字段',
                }
            },
            {
                path: 'update',
                name: '批量修改表字段',
                component: () => import("@/views/field/update"),
                meta:
                {
                    title: '批量修改表字段',
                }
            }
        ]
    },
    {
        path: 'data/opt',
        name: '表数据操作',
        component: () => import('@/views'),
        redirect: 'data/opt/delete',
        children: [
            {
                path: 'delete',
                name: '删除表数据',
                component: () => import("@/views/dataopt/delete"),
                meta:
                {
                    title: '删除表数据',
                }
            },
            {
                path: 'alter',
                name: '修改表数据',
                component: () => import("@/views/dataopt/alter"),
                meta:
                {
                    title: '修改表数据',
                }
            },
            {
                path: 'query',
                name: '查询表数据',
                component: () => import("@/views/dataopt/query"),
                meta:
                {
                    title: '查询表数据',
                }
            }
        ]
    },
    {
        path: 'data/transfer',
        name: '数据库迁移',
        component: () => import('@/views'),
        redirect: 'data/transfer/simple',
        children: [
            {
                path: 'simple',
                name: '简单数据迁移',
                component: () => import("@/views/transfer/simple"),
                meta:
                {
                    title: '简单数据迁移',
                }
            },
            {
                path: 'switchId',
                name: 'ID类型转换迁移',
                component: () => import("@/views/transfer/switchId"),
                meta:
                {
                    title: 'ID类型转换迁移',
                }
            }
        ]
    },
    {
        path: 'extend/utils',
        name: '扩展工具集',
        component: () => import('@/views'),
        redirect: 'extend/utils/sqltemple',
        children: [
            {
                path: 'sqltemple',
                name: '模板SQL器',
                component: () => import("@/views/extend/sqltemple"),
                meta:
                {
                    title: '模板SQL器',
                }
            },
            {
                path: 'differ',
                name: '表结构同步',
                component: () => import("@/views/extend/differ"),
                meta:
                {
                    title: '表结构同步',
                }
            },
            {
                path: 'dev-doc',
                name: '数据库文档',
                component: () => import("@/views/extend/dev-doc"),
                meta:
                {
                    title: '数据库文档',
                }
            },
            {
                path: 'file/analysis',
                name: '解析文件表',
                component: () => import("@/views/extend/analysis"),
                meta:
                {
                    title: '文件导入库',
                }
            }
        ]
    },
]