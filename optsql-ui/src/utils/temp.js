import menu from '@/config/menuinfo'

/**
 * @description 根据路由path获取菜单信息
 * @param {*} path 
 * @returns 
 * {
 *   parentId:'',
 *   childrenId: '',
 *   menuList: [{
 *     id: '',
 *     icon:'',
 *     name:'',
 *     path:"",
 *     component:"",
 *   }]
 * }
 */
export function getMenuInfoByPath(path) {
    const sideNav = menu.sideNav
    const subMenu = menu.subMenu
    let menuInfo = {}
    for (let item of subMenu) {
        menuInfo.parentId = item.parentId
        menuInfo.subList = item.menuList
        let menuList = item.menuList
        for (let mitem of menuList) {
            if (mitem.path == path) {
                menuInfo.childrenId = mitem.id
                return menuInfo
            }
        }
    }

    for (let item of sideNav) {
        if (item.path === path) {
            menuInfo.parentId = item.id
            for (let sitem of subMenu) {
                if (sitem.parentId == item.id) {
                    menuInfo.subList = sitem.menuList
                    menuInfo.childrenId = sitem.menuList[0].id
                    return menuInfo
                }
            }
            break
        }
    }
    return null
}