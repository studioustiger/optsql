import cache from '@/plugins/cache'

const TokenKey = 'Optsql-Token'

export function getToken() {
  return cache.local.get(TokenKey)
}

export function setToken(token) {
  return cache.local.set(TokenKey, token)
}

export function removeToken() {
  return cache.local.remove(TokenKey)
}
