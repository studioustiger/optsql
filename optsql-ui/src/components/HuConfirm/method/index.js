import confirm from '@/components/HuConfirm'

/**
 * @description 注入全局$huConfirm方法
 * @param {*} Vue 
 */
function installConfirm(Vue) {
  Object.defineProperty(Vue.prototype, '$huConfirm', {
    get() {
      return (options) => {
        const Constructor = Vue.extend(confirm)
        const Instance = new Constructor({
          data() {
            return {
              timeout: null,
              title: options == null || options.title == null ? '确认操作' : options.title,
              content: options == null || options.content == null ? '请确认是否继续？' : options.content,
              confirmText: options == null || options.confirmText == null ? '确认' : options.confirmText,
              cancelText: options == null || options.cancelText == null ? '取消' : options.cancelText,
              onlySubmit: options == null || options.onlySubmit == null ? false : options.onlySubmit,
              onlyClose: options == null || options.onlyClose == null ? false : options.onlyClose,
              onConfirm: options == null || options.onConfirm == null ? () => {} : options.onConfirm,
              onCancel: options == null || options.onCancel == null ? () => {} : options.onCancel,
            }
          }
        }).$mount(document.createElement('div'));
        document.body.appendChild(Instance.$el)
      };
    }
  })
}
export default installConfirm
