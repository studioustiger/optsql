/**
 * 注册自定义组件
 */

import installConfirm from '@/components/HuConfirm/method'
import installTip from '@/components/HuTip/method'

const HUnit = {
  install: (Vue) => {
    Vue.component('hu-empty', () => import("@/components/HuEmpty"))
    Vue.component('hu-avatar', () => import("@/components/HuAvatar"))
    // Vue.component('hu-back-top', () => import("@/components/HuBackTop"))
    Vue.component('hu-input-interval', () => import("@/components/HuInputInterval"))
    Vue.component('hu-select-datetime', () => import("@/components/HuSelectDatetime"))
    Vue.component('hu-select-month', () => import("@/components/HuSelectMonth"))
    Vue.component('hu-select-year', () => import("@/components/HuSelectYear"))
    Vue.component('hu-select-tree', () => import("@/components/HuSelectTree"))
    Vue.component('hu-dialog', () => import("@/components/HuDialog"))
    Vue.component('hu-table', () => import("@/components/HuTable"))
    Vue.component('hu-select-dict', () => import("@/components/HuSelectDict"))
    Vue.component('hu-carousel', () => import("@/components/HuCarousel"))
    Vue.component('hu-input-textarea', () => import("@/components/HuInputTextarea"))
    Vue.component('hu-input-password', () => import("@/components/HuInputPassword"))
    Vue.component('hu-input-count', () => import("@/components/HuInputCount"))
    Vue.component('hu-input-bool', () => import("@/components/HuInputBool"))
    Vue.component('hu-skeleton', () => import("@/components/HuSkeleton"))
    Vue.component('hu-loading', () => import("@/components/HuLoading"))
    Vue.component('hu-input', () => import("@/components/HuInput"))
    Vue.component('hu-tag', () => import("@/components/HuTag"))
    Vue.component('hu-button', () => import("@/components/HuButton"))
    Vue.component('hu-page', () => import("@/components/HuPage"))
    Vue.component('hu-editor', () => import("@/components/HuEditor"))
    Vue.use(installConfirm)
    Vue.use(installTip)
  }
}

export default HUnit
