import tip from '@/components/HuTip'

/**
 * @description 注入全局$huTip方法
 * @param {*} Vue 
 */
function installTip(Vue) {
  Object.defineProperty(Vue.prototype, '$huTip', {
    get() {
      return (options) => {
        const Constructor = Vue.extend(tip)
        const Instance = new Constructor({
          data() {
            return {
              timeout: null,
              value: options == null || options.value == null ? '操作提示' : options.value,
              type: options == null || options.type == null ? 'info' : options.type,
              color: 'rgba(212, 212, 212, 0.6)'
            }
          }
        }).$mount(document.createElement('div'));
        document.body.appendChild(Instance.$el)
      };
    }
  })
  Object.defineProperty(Vue.prototype, '$huTip_info', {
    get() {
      return (value) => {
        const Constructor = Vue.extend(tip)
        const Instance = new Constructor({
          data() {
            return {
              timeout: null,
              value: value == null ? '操作提示' : value,
              type: 'info',
              color: 'rgba(212, 212, 212, 0.6)'
            }
          }
        }).$mount(document.createElement('div'));
        document.body.appendChild(Instance.$el)
      };
    }
  })
  Object.defineProperty(Vue.prototype, '$huTip_error', {
    get() {
      return (value) => {
        const Constructor = Vue.extend(tip)
        const Instance = new Constructor({
          data() {
            return {
              timeout: null,
              value: value == null ? '操作失败' : value,
              type: 'error',
              color: 'rgba(180, 41, 41, 0.6)'
            }
          }
        }).$mount(document.createElement('div'));
        document.body.appendChild(Instance.$el)
      };
    }
  })
  Object.defineProperty(Vue.prototype, '$huTip_success', {
    get() {
      return (value) => {
        const Constructor = Vue.extend(tip)
        const Instance = new Constructor({
          data() {
            return {
              timeout: null,
              value: value == null ? '操作成功' : value,
              type: 'success',
              color: 'rgba(27, 169, 70, 0.6)'
            }
          }
        }).$mount(document.createElement('div'));
        document.body.appendChild(Instance.$el)
      };
    }
  })
  Object.defineProperty(Vue.prototype, '$huTip_warning', {
    get() {
      return (value) => {
        const Constructor = Vue.extend(tip)
        const Instance = new Constructor({
          data() {
            return {
              timeout: null,
              value: value == null ? '操作警告' : value,
              type: 'warning',
              color: 'rgba(179, 136, 18, 0.6)'
            }
          }
        }).$mount(document.createElement('div'));
        document.body.appendChild(Instance.$el)
      };
    }
  })

}
export default installTip
