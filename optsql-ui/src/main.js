import Vue from 'vue'
import App from './App.vue'

import '@/assets/styles/minireset.css'
import '@/assets/styles/normalize.css'
import '@/assets/icons'
import '@/components'
import '@/plugins/fullscreen'

import store from './store'
import router from './router'
import '@/router/intercept.js'

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
}).$mount('#app')
