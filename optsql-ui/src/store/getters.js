const getters = {
    sidebar: state => state.menu.sidebar,
    subMenu: state => state.menu.subMenu,
    tags: state => state.menu.tags,
    setting: state => state.setting.setting,
    userInfo: state => state.user.userInfo,
}
export default getters