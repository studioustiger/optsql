import Vue from 'vue'
import Vuex from 'vuex'
import menu from './modules/menu'
import setting from './modules/setting'
import user from './modules/user'
import getters from './getters'

Vue.use(Vuex)

const store = new Vuex.Store(
    {
        modules:
        {
            menu,
            setting,
            user
        },
        getters
    }
)

export default store
