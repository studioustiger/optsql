import cache from '@/plugins/cache'

const state = {
    setting:
    {
        /* 搜索可用 */
        ableSearch: cache.local.get('settingAbleSearch') ? cache.local.get('sidebarIsOpen') : 1,
        /* 锁屏可用 */
        ableLock: cache.local.get('settingAbleLock') ? cache.local.get('sidebarIsOpen') : 1,
        /* 全屏可用 */
        ableFull: cache.local.get('settingAbleFull') ? cache.local.get('sidebarIsOpen') : 1,
        /* 主题 */
        theme: cache.local.get('settingTheme') ? cache.local.get('sidebarIsOpen') : 1
    }
}

const mutations = {
    CHANGE_SEARCH: (state, val) => {
        state.setting.ableSearch = val
        cache.local.set('settingAbleSearch', val)
    },
    CHANGE_LOCK: (state, val) => {
        state.setting.ableLock = val
        cache.local.set('settingAbleLock', val)
    },
    CHANGE_FULL: (state, val) => {
        state.setting.ableFull = val
        cache.local.set('settingAbleFull', val)
    },
    CHANGE_THEME: (state, val) => {
        state.setting.theme = val
        cache.local.set('settingTheme', val)
    },
}

const actions = {
    search({ commit }, val) {
        commit('CHANGE_SEARCH', val)
    },

    lock({ commit }, val) {
        commit('CHANGE_LOCK', val)
    },

    full({ commit }, val) {
        commit('CHANGE_FULL', val)
    },

    theme({ commit }, val) {
        commit('CHANGE_THEME', val)
    },
}

export default
    {
        namespaced: true,
        state,
        mutations,
        actions
    }