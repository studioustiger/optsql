const IS_PROD = ["production", "prod"].includes(process.env.NODE_ENV);

//引入path模块，为设置别名做准备
const path = require('path')
const resolve = (dir) => {
  // path.join(__dirname)设置绝对路径
  return path.join(__dirname, dir);
};

// 开启gzip压缩，优化打包体积，提升加载速度
// const CompressionPlugin = require('compression-webpack-plugin')
// import website from './src/config/website'
const website = require('./src/config/website');

module.exports = {
  // 应用的基本 URL
  // publicPath: './',
  publicPath: website.mode === 'web' ? process.env.VUE_APP_BASE_URL : './',
  // 在npm run build 或 yarn build 时 ，生成文件的目录名称（要和baseUrl的生产环境路径一致）（默认dist）
  outputDir: process.env.outputDir || 'dist',
  // 用于放置生成的静态资源 (js、css、img、fonts) 的；（项目打包之后，静态资源会放在这个文件夹下）
  assetsDir: "static",
  // 是否开启eslint保存检测，有效值：ture | false | 'error'
  lintOnSave: false,
  // 是否使用包含运行时编译器的 Vue 构建版本
  runtimeCompiler: true,
  // 生产环境的 source map
  productionSourceMap: !IS_PROD,
  parallel: require("os").cpus().length > 1,
  pwa: {},

  devServer: {
    // 让浏览器 overlay 同时显示警告和错误
    overlay: {
      warnings: true,
      errors: true
    },
    // 是否打开浏览器
    open: false,
    // 保证同局域网下的其他机器可以通过本机ip访问服务，改成localhost或者127.0.0.1后就无法访问
    host: '0.0.0.0',
    port: "8081",
    https: false,
    // 热更新
    // hotOnly: false,

    // 代理
    proxy: {
      ['^' + process.env.VUE_APP_BASE_API]: {
        // 目标代理接口地址
        target:
          "http://localhost:8080",
        secure: false,
        // 开启代理，在本地创建一个虚拟服务端,保证发出的请求IP端口一致，从而解决跨域问题
        changeOrigin: true,
        // 是否启用websockets
        // ws: true,
        pathRewrite: {
          ['^' + process.env.VUE_APP_BASE_API]: ''
        }
      }
    }

  },

  css: {
    loaderOptions: {
      sass: {
        // sassOptions: { outputStyle: "expanded" },
        //引入全局变量
        data: `@import "@/assets/scss/_variable.scss";`
      }
    }
  },

  configureWebpack: {
    resolve: {
      // 添加别名
      alias: {
        '@': resolve('src'),
        '@assets': resolve('src/assets'),
        '@components': resolve('src/components'),
        '@plugins': resolve('src/plugins'),
        '@views': resolve('src/views'),
        '@router': resolve('src/router'),
        '@store': resolve('src/store'),
        '@layout': resolve('src/layout'),
      }
    },

    // plugins: [
    //   /**
    //    * 开启gzip压缩，优化打包体积，提升加载速度。
    //    * 最后，在部署的时候，我们需要在nginx上开启gzip.
    //    */
    //   new CompressionPlugin({
    //     cache: false,                   // 不启用文件缓存
    //     test: /\.(js|css|html)?$/i,     // 压缩文件格式
    //     filename: '[path].gz[query]',   // 压缩后的文件名
    //     algorithm: 'gzip',              // 使用gzip压缩
    //     minRatio: 0.8                   // 压缩率小于1才会压缩
    //   })
    // ],
  },

  chainWebpack: config => {
    // 修复HMR(热更新)失效
    config.resolve.symlinks(true);

    // 如果使用多页面打包，使用vue inspect --plugins查看html是否在结果数组中
    config.plugin("html").tap(args => {
      // 修复 Lazy loading routes Error
      args[0].chunksSortMode = "none";
      return args;
    });

    // 设置 svg-sprite-loader
    config.module
      .rule('svg')
      .exclude.add(resolve('src/assets/icons'))
      .end()
    config.module
      .rule('icons')
      .test(/\.svg$/)
      .include.add(resolve('src/assets/icons'))
      .end()
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]'
      })
      .end()
  },
};
