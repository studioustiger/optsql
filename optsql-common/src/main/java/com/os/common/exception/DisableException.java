package com.os.common.exception;

import com.os.common.enums.ResponseStatus;
import com.os.common.exception.base.BaseException;

/**
 * 描述：禁用异常
 *
 * @author huxuehao
 **/
public class DisableException extends BaseException {
    public DisableException(int code, String module, String method, String message) {
        super(code, module, method, message);
    }

    public DisableException(String module, String method, String message) {
        super(ResponseStatus.Disable.code, module, method, message);
    }

    public DisableException(String message) {
        super(ResponseStatus.Disable.code, null, null, message);
    }

    public DisableException() {
        super(ResponseStatus.Disable.code, null, null, ResponseStatus.Disable.msg);
    }
}
