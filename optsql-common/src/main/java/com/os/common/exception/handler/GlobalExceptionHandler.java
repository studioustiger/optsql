package com.os.common.exception.handler;

import com.os.common.exception.*;
import com.os.common.response.Response;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.context.annotation.Description;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.naming.NotContextException;
import java.sql.SQLException;

/**
 * 描述：全局异常处理
 *
 * @author huxuehao
 **/
@ControllerAdvice
public class GlobalExceptionHandler {
    @ResponseBody
    @Description(value = "内部错误")
    @ExceptionHandler(value = Exception.class)
    public Response exceptionHandler(Exception e) {
        e.printStackTrace();
        return Response.error(500, "系统内部错误");
    }

    @ResponseBody
    @Description(value = "空指针异常")
    @ExceptionHandler(value = NullPointerException.class)
    public Response exceptionHandler(NullPointerException e) {
        e.printStackTrace();
        return Response.error(500, "空指针异常");
    }

    @ResponseBody
    @Description(value = "SQL异常处理")
    @ExceptionHandler(value = SQLException.class)
    public Response exceptionHandler(SQLException e) {
        e.printStackTrace();
        return Response.error(500, e.getMessage());
    }

    @ResponseBody
    @Description(value = "错误请求异常处理")
    @ExceptionHandler(value = BadRequestException.class)
    public Response exceptionHandler(BadRequestException e) {
        return Response.error(e.getCode(), e.getMessage());
    }

    @ResponseBody
    @Description(value = "不可用异常处理")
    @ExceptionHandler(value = DisableException.class)
    public Response exceptionHandler(DisableException e) {
        return Response.error(e.getCode(), e.getMessage());
    }

    @ResponseBody
    @Description(value = "内部系统错误异常处理")
    @ExceptionHandler(value = ErrorException.class)
    public Response exceptionHandler(ErrorException e) {
        return Response.error(e.getCode(), e.getMessage());
    }

    @ResponseBody
    @Description(value = "禁止访问异常处理")
    @ExceptionHandler(value = ForbiddenException.class)
    public Response exceptionHandler(ForbiddenException e) {
        return Response.error(e.getCode(), e.getMessage());
    }

    @ResponseBody
    @Description(value = "登录错误处理")
    @ExceptionHandler(value = ExpiredJwtException.class)
    public Response exceptionHandler(ExpiredJwtException e) {
        return Response.error(401, "登录过期，请重新登录");
    }

    @ResponseBody
    @Description(value = "方法不允许异常处理")
    @ExceptionHandler(value = MethodNotAllowedException.class)
    public Response exceptionHandler(MethodNotAllowedException e) {
        return Response.error(e.getCode(), e.getMessage());
    }

    @ResponseBody
    @Description(value = "没有内容异常处理")
    @ExceptionHandler(value = NoContentException.class)
    public Response exceptionHandler(NoContentException e) {
        return Response.error(e.getCode(), e.getMessage());
    }

    @ResponseBody
    @Description(value = "权限异常处理")
    @ExceptionHandler(value = NotAuthException.class)
    public Response exceptionHandler(NotAuthException e) {
        return Response.error(e.getCode(), e.getMessage());
    }

    @ResponseBody
    @Description(value = "接口没有进行权限标记异常处理")
    @ExceptionHandler(value = InterfaceNotAuthException.class)
    public Response exceptionHandler(InterfaceNotAuthException e) {
        return Response.error(e.getCode(), e.getMessage());
    }

    @ResponseBody
    @Description(value = "未实现异常处理")
    @ExceptionHandler(value = NotCompleteException.class)
    public Response exceptionHandler(NotCompleteException e) {
        return Response.error(e.getCode(), e.getMessage());
    }

    @ResponseBody
    @Description(value = "未找到异常处理")
    @ExceptionHandler(value = NotFoundException.class)
    public Response exceptionHandler(NotFoundException e) {
        return Response.error(e.getCode(), e.getMessage());
    }

    @ResponseBody
    @Description(value = "请求超时异常处理")
    @ExceptionHandler(value = TimeoutException.class)
    public Response exceptionHandler(TimeoutException e) {
        return Response.error(e.getCode(), e.getMessage());
    }

    @ResponseBody
    @Description(value = "为找到数据源异常处理")
    @ExceptionHandler(value = DataSourceNotFoundException.class)
    public Response exceptionHandler(DataSourceNotFoundException e) {
        return Response.error(e.getCode(), e.getMessage());
    }

    @ResponseBody
    @Description(value = "数据源创建失败")
    @ExceptionHandler(value = DataSourceCreateException.class)
    public Response exceptionHandler(DataSourceCreateException e) {
        return Response.error(e.getCode(), e.getMessage());
    }

    @ResponseBody
    @Description(value = "数据源配置存在错误")
    @ExceptionHandler(value = DataSourceConfigException.class)
    public Response exceptionHandler(DataSourceConfigException e) {
        return Response.error(e.getCode(), e.getMessage());
    }

    @ResponseBody
    @Description(value = "为找到数据源Id异常")
    @ExceptionHandler(value = NotGetDsAnnotationException.class)
    public Response exceptionHandler(NotGetDsAnnotationException e) {
        e.printStackTrace();
        return Response.error(500, e.getMessage());
    }

    @ResponseBody
    @Description(value = "未找到内容")
    @ExceptionHandler(value = NotContextException.class)
    public Response exceptionHandler(NotContextException e) {
        e.printStackTrace();
        return Response.error(500, e.getMessage());
    }
}
