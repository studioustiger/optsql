package com.os.common.exception;

import com.os.common.enums.ResponseStatus;
import com.os.common.exception.base.BaseException;

/**
 * 描述：方法不允许访问异常
 *
 * @author huxuehao
 **/
public class MethodNotAllowedException extends BaseException {
    public MethodNotAllowedException(int code, String module, String method, String message) {
        super(code, module, method, message);
    }

    public MethodNotAllowedException(String module, String method, String message) {
        super(ResponseStatus.MethodNotAllowed.code, module, method, message);
    }

    public MethodNotAllowedException(String message) {
        super(ResponseStatus.MethodNotAllowed.code, null, null, message);
    }

    public MethodNotAllowedException() {
        super(ResponseStatus.MethodNotAllowed.code, null, null, ResponseStatus.MethodNotAllowed.msg);
    }
}
