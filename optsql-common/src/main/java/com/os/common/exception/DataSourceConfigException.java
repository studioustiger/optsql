package com.os.common.exception;

import com.os.common.enums.ResponseStatus;
import com.os.common.exception.base.BaseException;

/**
 * 描述：数据源配置异常
 *
 * @author huxuehao
 **/
public class DataSourceConfigException extends BaseException {
    public DataSourceConfigException(int code, String module, String method, String message) {
        super(code, module, method, message);
    }

    public DataSourceConfigException(String module, String method, String message) {
        super(ResponseStatus.DataSourceConfigError.code, module, method, message);
    }

    public DataSourceConfigException(String message) {
        super(ResponseStatus.DataSourceConfigError.code, null, null, message);
    }

    public DataSourceConfigException() {
        super(ResponseStatus.DataSourceConfigError.code, null, null, ResponseStatus.DataSourceConfigError.msg);
    }
}