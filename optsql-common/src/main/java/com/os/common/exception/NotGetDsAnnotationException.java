package com.os.common.exception;

import com.os.common.enums.ResponseStatus;
import com.os.common.exception.base.BaseException;

/**
 * 描述：没有找到数据源注解
 *
 * @author huxuehao
 **/
public class NotGetDsAnnotationException extends BaseException {
    public NotGetDsAnnotationException(int code, String module, String method, String message) {
        super(code, module, method, message);
    }

    public NotGetDsAnnotationException(String module, String method, String message) {
        super(ResponseStatus.NotGetDsAnnotation.code, module, method, message);
    }

    public NotGetDsAnnotationException(String message) {
        super(ResponseStatus.NotGetDsAnnotation.code, null, null, message);
    }

    public NotGetDsAnnotationException() {
        super(ResponseStatus.NotGetDsAnnotation.code, null, null, ResponseStatus.NotGetDsAnnotation.msg);
    }
}
