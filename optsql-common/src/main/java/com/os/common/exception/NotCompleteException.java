package com.os.common.exception;

import com.os.common.enums.ResponseStatus;
import com.os.common.exception.base.BaseException;

/**
 * 描述：功能未完成异常
 *
 * @author huxuehao
 **/
public class NotCompleteException extends BaseException {
    public NotCompleteException(int code, String module, String method, String message) {
        super(code, module, method, message);
    }

    public NotCompleteException(String module, String method, String message) {
        super(ResponseStatus.NotComplete.code, module, method, message);
    }

    public NotCompleteException(String message) {
        super(ResponseStatus.NotComplete.code, null, null, message);
    }

    public NotCompleteException() {
        super(ResponseStatus.NotComplete.code, null, null, ResponseStatus.NotComplete.msg);
    }
}
