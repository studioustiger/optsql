package com.os.common.exception;

import com.os.common.enums.ResponseStatus;
import com.os.common.exception.base.BaseException;

/**
 * 描述：数据源创建异常
 *
 * @author huxuehao
 **/
public class DataSourceCreateException extends BaseException {
    public DataSourceCreateException(int code, String module, String method, String message) {
        super(code, module, method, message);
    }

    public DataSourceCreateException(String module, String method, String message) {
        super(ResponseStatus.DataSourceCreateError.code, module, method, message);
    }

    public DataSourceCreateException(String message) {
        super(ResponseStatus.DataSourceCreateError.code, null, null, message);
    }

    public DataSourceCreateException() {
        super(ResponseStatus.DataSourceCreateError.code, null, null, ResponseStatus.DataSourceCreateError.msg);
    }
}