package com.os.common.exception;

import com.os.common.enums.ResponseStatus;
import com.os.common.exception.base.BaseException;

/**
 * 描述：数据源未找到异常
 *
 * @author huxuehao
 **/
public class DataSourceNotFoundException extends BaseException {
    public DataSourceNotFoundException(int code, String module, String method, String message) {
        super(code, module, method, message);
    }

    public DataSourceNotFoundException(String module, String method, String message) {
        super(ResponseStatus.DataSourceNotFound.code, module, method, message);
    }

    public DataSourceNotFoundException(String message) {
        super(ResponseStatus.DataSourceNotFound.code, null, null, message);
    }

    public DataSourceNotFoundException() {
        super(ResponseStatus.DataSourceNotFound.code, null, null, ResponseStatus.DataSourceNotFound.msg);
    }
}