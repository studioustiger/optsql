package com.os.common.exception;

import com.os.common.enums.ResponseStatus;
import com.os.common.exception.base.BaseException;

/**
 * 描述：内部服务器错误
 *
 * @author huxuehao
 **/
public class ErrorException extends BaseException {
    public ErrorException(int code, String module, String method, String message) {
        super(code, module, method, message);
    }

    public ErrorException(String module, String method, String message) {
        super(ResponseStatus.Error.code, module, method, message);
    }

    public ErrorException(String message) {
        super(ResponseStatus.Error.code, null, null, message);
    }

    public ErrorException() {
        super(ResponseStatus.Error.code, null, null, ResponseStatus.Error.msg);
    }
}
