package com.os.common.exception;

import com.os.common.enums.ResponseStatus;
import com.os.common.exception.base.BaseException;

/**
 * 描述：错误请求异常
 *
 * @author huxuehao
 **/
public class BadRequestException extends BaseException {
    public BadRequestException(int code, String module, String method, String message) {
        super(code, module, method, message);
    }

    public BadRequestException(String module, String method, String message) {
        super(ResponseStatus.BadRequest.code, module, method, message);
    }

    public BadRequestException(String message) {
        super(ResponseStatus.BadRequest.code, null, null, message);
    }

    public BadRequestException() {
        super(ResponseStatus.BadRequest.code, null, null, ResponseStatus.BadRequest.msg);
    }
}
