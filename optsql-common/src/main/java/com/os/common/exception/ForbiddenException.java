package com.os.common.exception;

import com.os.common.enums.ResponseStatus;
import com.os.common.exception.base.BaseException;

/**
 * 描述：禁止访问异常
 *
 * @author huxuehao
 **/
public class ForbiddenException extends BaseException {
    public ForbiddenException(int code, String module, String method, String message) {
        super(code, module, method, message);
    }

    public ForbiddenException(String module, String method, String message) {
        super(ResponseStatus.Forbidden.code, module, method, message);
    }

    public ForbiddenException(String message) {
        super(ResponseStatus.Forbidden.code, null, null, message);
    }

    public ForbiddenException() {
        super(ResponseStatus.Forbidden.code, null, null, ResponseStatus.Forbidden.msg);
    }
}
