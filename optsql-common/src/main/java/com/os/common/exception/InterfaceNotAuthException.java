package com.os.common.exception;

import com.os.common.enums.ResponseStatus;
import com.os.common.exception.base.BaseException;

/**
 * 描述：请求未授权异常
 *
 * @author huxuehao
 **/
public class InterfaceNotAuthException extends BaseException {
    public InterfaceNotAuthException(int code, String module, String method, String message) {
        super(code, module, method, message);
    }

    public InterfaceNotAuthException(String module, String method, String message) {
        super(ResponseStatus.InterfaceNotAuth.code, module, method, message);
    }

    public InterfaceNotAuthException(String message) {
        super(ResponseStatus.InterfaceNotAuth.code, null, null, message);
    }

    public InterfaceNotAuthException() {
        super(ResponseStatus.InterfaceNotAuth.code, null, null, ResponseStatus.InterfaceNotAuth.msg);
    }
}
