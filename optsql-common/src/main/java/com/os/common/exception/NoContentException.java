package com.os.common.exception;

import com.os.common.enums.ResponseStatus;
import com.os.common.exception.base.BaseException;

/**
 * 描述：没有内容异常
 *
 * @author huxuehao
 **/
public class NoContentException extends BaseException {
    public NoContentException(int code, String module, String method, String message) {
        super(code, module, method, message);
    }

    public NoContentException(String module, String method, String message) {
        super(ResponseStatus.NoContent.code, module, method, message);
    }

    public NoContentException(String message) {
        super(ResponseStatus.NoContent.code, null, null, message);
    }

    public NoContentException() {
        super(ResponseStatus.NoContent.code, null, null, ResponseStatus.NoContent.msg);
    }
}
