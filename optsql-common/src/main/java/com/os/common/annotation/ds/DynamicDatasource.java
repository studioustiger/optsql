package com.os.common.annotation.ds;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 描述：用于方法，其作用是标记是否切换动态数据源
 *
 * @author huxuehao
 **/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DynamicDatasource {
    String value() default "datasourceId";
}
