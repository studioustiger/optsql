package com.os.common.utils;
 
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 描述：日期时间转换工具类
 *
 * @author huxuehao
 **/
public class DatetimeUtil {
    private static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final String DATE_FORMAT = "yyyy-MM-dd";

    /**
     * 当前sql类型Timestamp的当前时间
     */
    public static Timestamp currentDateTime2SQLTimestamp() {
        Date utilDate = new Date();
        return new Timestamp(utilDate.getTime());
    }

    /**
     * 数据库datetime转String
     * @param sqlDateTime 时间戳
     */
    public static String sqlDateTime2String(Timestamp sqlDateTime) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATETIME_FORMAT);
        return sdf.format(sqlDateTime);
    }

    /** 将long类型格式化 */
    public static String long2FormString(long lo) {
        Date date = new Date(lo);
        SimpleDateFormat sd = new SimpleDateFormat(DATETIME_FORMAT);
        return sd.format(date);
    }
    /**
     * 将数据库 Timestamp 格式化 (yyyy-MM-dd HH:mm:ss)
     * @param sqlDateTime 时间戳
     */
    public static String sqlTimestamp2FormStr(Timestamp sqlDateTime) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATETIME_FORMAT);
        return sdf.format(sqlDateTime);
    }

    /**
     * 获取当前已经格式化后的时间 (yyyy-MM-dd HH:mm:ss)
     */
    public static String currentDatetimeOfFormStr() {
        Timestamp timestamp = currentDateTime2SQLTimestamp();
        return sqlTimestamp2FormStr(timestamp);
    }

    /**
     * 获取当前已经格式化后的日期 (yyyyMMdd)
     */
    public static String currentDateOfFormStr() {
        Timestamp timestamp = currentDateTime2SQLTimestamp();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        return sdf.format(timestamp);
    }
}