package com.os.common.entity.foreignkey;

import com.os.common.entity.base.BaseEntity;
import lombok.*;

import java.util.List;

/**
 * 描述：外键信息
 *
 * @author huxuehao
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ForeignKeyInfo extends BaseEntity {
    private String filterSQL; /* 删除数据时的过滤语句 */
    private List<RefTableNameAndColumnName> foreignKeyInfos; /* 外键信息 */
}
