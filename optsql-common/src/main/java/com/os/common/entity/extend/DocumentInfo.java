package com.os.common.entity.extend;

import com.os.common.entity.base.BaseEntity;
import lombok.*;

import java.util.List;

/**
 * @ClassName DocumentInfo
 * @Description TODO
 * @Author huxuehao 匹配
 **/
/**
 * 描述：用于方法，其作用是标记是否切换动态数据源
 *
 * @author huxuehao
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DocumentInfo extends BaseEntity {
    private String datasourceId;
    private String fileName;
    private String description;
    private String version;
//    private String fileOutputDir;
    private List<String> ignoreTableName;
    private List<String> ignorePrefix;
    private List<String> ignoreSuffix;
}
