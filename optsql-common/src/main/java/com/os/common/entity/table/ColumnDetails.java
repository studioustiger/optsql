package com.os.common.entity.table;

import com.os.common.entity.base.BaseEntity;
import lombok.*;

/**
 * 描述：
 *
 * @author huxuehao
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ColumnDetails extends BaseEntity {
    private String tableSchema;
    private String tableName;
    private String columnName;
    private String columnType;
    private String columnComment;
}
