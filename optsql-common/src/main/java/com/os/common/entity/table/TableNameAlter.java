package com.os.common.entity.table;

import com.os.common.entity.base.BaseEntity;
import lombok.*;

import java.util.List;

/**
 * 描述：修改表名实体类 # AlterTableNameDescribe
 *
 * @author huxuehao
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TableNameAlter extends BaseEntity {
    private List<String> originTableName;
    /**
     * optType:
     *      insertPrefix : 插入前缀
     *      insertSuffix ： 插入后缀
     *      replaceAll ： 替换全部
     *      replaceFirst ： 替换第一个
     *      replaceLast ： 替换最后一个
     */
    private String optType;
    /* 存放前缀/后缀/替换字段 */
    private String newSubName;
    /* 存放被替换字段 */
    private String replacedSubName;
    private String datasourceId;

}
