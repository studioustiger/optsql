package com.os.common.entity.table;

import com.os.common.entity.base.BaseEntity;
import lombok.*;

/**
 * 描述：删除表数据的实体类 # DeleteTableRecordDesc
 *
 * @author huxuehao
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TableRecordDel extends BaseEntity {
    private String tableName; /* 表名 */
    private String filterSQL; /* 过滤语句 */
}
