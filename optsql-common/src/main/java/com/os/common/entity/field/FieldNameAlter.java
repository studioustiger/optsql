package com.os.common.entity.field;

import com.os.common.entity.base.BaseEntity;
import lombok.*;

import java.util.ArrayList;

/**
 * 描述：给指定字段添加前后缀的实体类
 *
 * @author huxuehao
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FieldNameAlter extends BaseEntity {
    private String rule; /* 规则：两种规则prefix或suffix */
    private String tableName; /* 表名 */
    private String subFiledValue; /* 前缀或后缀值 */
    private ArrayList<String> fieldNames; /* 字段名 */
}
