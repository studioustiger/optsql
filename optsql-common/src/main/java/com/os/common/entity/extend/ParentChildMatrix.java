package com.os.common.entity.extend;

import com.os.common.entity.base.BaseEntity;
import lombok.*;

/**
 * 描述： 获取前指定表的父子矩阵参数类
 *
 * @author huxuehao
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ParentChildMatrix extends BaseEntity {
    private String tableName; /* 表名 */
    private String showFieldName; /* 矩阵展示时的字段 */
    private String parentFieldName; /* 主键 */
    private String childFieldName; /* 父字段名 */
    private Integer matrixLevel; /* 字典等级 */
    private String filterSQL; /* 过滤SQL */
}
