package com.os.common.entity.field;

import com.os.common.entity.base.BaseEntity;
import lombok.*;

import java.util.List;

/**
 * 描述：修改字段的描述
 *
 * @author huxuehao
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FieldAlter extends BaseEntity {
    /** 表名*/
    private List<String> tableNames;
    /** 匹配的正则表达式 */
    private String matchRegexp;
    private String newFieldName;
    private String newFieldType;
    private String newFieldComment;
    private String newFieldLength;
    private Boolean delete;
    /**
     * 匹配依据：
     * 10：表示根据fieldName进行修改
     * 20：表示根据fieldType进行修改
     * 30：表示根据fieldComment进行修改
     */
    private Integer matchBasis;
    private String datasourceId;

}
