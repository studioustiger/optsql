package com.os.common.entity.system;

import com.os.common.entity.base.BaseEntity;
import lombok.*;

/**
 * 描述：
 *
 * @author huxuehao
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SysSetting extends BaseEntity {
    private String id;
    private String code;
    private String name;
    private int enable;
    private int delFlag;
}
