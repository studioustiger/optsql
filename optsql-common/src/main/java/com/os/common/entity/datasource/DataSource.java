package com.os.common.entity.datasource;

import com.os.common.entity.base.BaseEntity;
import lombok.*;

/**
 * @ClassName DataSource
 * @Description 动态数据源
 * @Author StudiousTiger
 **/
/**
 * 描述：用于方法，其作用是标记是否切换动态数据源
 *
 * @author huxuehao
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DataSource extends BaseEntity {
    private String id;
    private String name;
    private String drive;
    private String url;
    private String userName;
    private String password;
    private String databaseType;
    private String code;
    private Integer delFlag;
}
