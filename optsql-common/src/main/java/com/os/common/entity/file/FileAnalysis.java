package com.os.common.entity.file;

import com.os.common.entity.base.BaseEntity;
import lombok.*;

import java.util.List;

/**
 * 描述：存放对文件（excel、csv、txt）分析后的结果
 *
 * @author huxuehao
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FileAnalysis extends BaseEntity {
    private String defaultTableName; /* 默认表名 */
    private List<Object> defaultFieldName; /* 默认字段名集合*/
    private List<Object> defaultFieldType; /* 默认字段类型集合 */
    private List<List<Object>> dataList; /* 待插入数据集合 */
    private List<Object> primaryKey; /* 主键集合 */
}
