package com.os.common.entity.extend;

import com.os.common.entity.base.BaseEntity;
import lombok.*;

/**
 * 描述：ID类型转换规则实体类
 *
 * @author huxuehao
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SwitchIDType extends BaseEntity {
    /** 旧的数据类型 */
    private String oldIdType;
    /** 可以为空，SwitchMode为1时，默认为bigint(20),SwitchMode为2时，默认为varchar(40) */
    private String newIdType;
    /**
     * 1 表示将旧的ID转换成雪花ID
     * 2 表示将旧的Id转换成UUID（UUID去除‘-’）
     */
    private Integer switchMode;
}
