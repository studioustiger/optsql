package com.os.common.entity.extend;

import lombok.Getter;
import lombok.Setter;

/**
 * 描述：用于方法，其作用是标记是否切换动态数据源
 *
 * @author huxuehao
 **/
@Getter
@Setter
public class ColumnInfo {
    private String name;
    private String type;
    private String nullAble;
    private String defaultVal;
    private String comment;

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ColumnInfo) {
            ColumnInfo ci = (ColumnInfo)obj;
            return this.name.equalsIgnoreCase(ci.getName()) && this.type.equalsIgnoreCase(ci.getType());
        } else {
            return false;
        }
    }
}
