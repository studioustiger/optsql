package com.os.common.entity.extend;

import com.os.common.entity.base.BaseEntity;
import lombok.*;

/**
 * 描述：表结构同步实体类
 *
 * @author huxuehao
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TableMatch extends BaseEntity {
    private String sourceDS; /*源数据源*/
    private String targetDS; /*目标数据源*/
    private String lessTableOpt; /*目标库中少表时的操作：ignore（忽略）、add（添加）*/
    private String moreTableOpt; /*目标库中多表时的操作：ignore（忽略）、delete（删除）*/
    private String lessColOpt; /*目标库中表少时的字段的操作：ignore（忽略）、add（添加）*/
    private String moreColOpt; /*目标库中表多时的字段的操作：ignore（忽略）、delete（删除）*/
}
