package com.os.common.entity.base;

import java.io.Serializable;

/**
 * @ClassName BaseEntity
 * @Description TODO
 * @Author StudiousTiger
 **/
/**
 * 描述：用于方法，其作用是标记是否切换动态数据源
 *
 * @author huxuehao
 **/
public class BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

}
