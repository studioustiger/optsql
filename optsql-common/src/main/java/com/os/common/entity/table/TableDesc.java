package com.os.common.entity.table;

import com.os.common.entity.base.BaseEntity;
import lombok.*;

/**
 * 描述：表的描述实体 # TableDescribe
 *
 * @author huxuehao
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TableDesc extends BaseEntity {
    /**
     * 表格目录
     */
    private String tableCatalog;
    /**
     * 表模式
     */
    private String tableSchema;

    /**
     * 表名称
     */
    private String tableName;

    /**
     * 表类型
     */
    private String tableType;

    /**
     * 引擎
     */
    private String engine;

    /**
     * 行格式
     */
    private String rowFormat;

    /**
     * 表行数
     */
    private String tableRows;

    /**
     * 平均行长度
     */
    private String avgRowLength;

    /**
     * 数据长度
     */
    private String dataLength;

    /**
     * 最大数据长度
     */
    private String maxDataLength;

    /**
     * 索引长度
     */
    private String indexLength;

    /**
     * 无数据
     */
    private String dataFree;

    /**
     * 自增
     */
    private String autoIncrement;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 更新时间
     */
    private String updateTime;

    /**
     * 检查时间
     */
    private String checkTime;

    /**
     * 表排序规则
     */
    private String tableCollation;

    /**
     * 校验和
     */
    private String checksum;

    /**
     * 创建选项
     */
    private String createOptions;

    /**
     * 表注释
     */
    private String tableComment;
}
