package com.os.common.entity.field;

import com.os.common.entity.base.BaseEntity;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 描述：合并字段规则实体类
 *
 * @author huxuehao
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FieldMerge extends BaseEntity {
    private String rule; /* 合并到新字段:mergeNew；合并到已存在字段:mergeExist */
    private String tableName; /* 被操作表 */
    private String mergeFieldTo; /* 最终合并到哪一个字段（没有字段则穿件字段，有字段则更新字段） */
    private String mergeFieldType;  /* 字段类型 */
    private String regx; /* 连接符 */
    private List<String> fieldNames; /* 待合并的字段名 */
}
