package com.os.common.entity.foreignkey;

import com.os.common.entity.base.BaseEntity;
import lombok.*;

/**
 * 描述：表的字段对应的外键表和外键表的字段
 *
 * @author huxuehao
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RefTableNameAndColumnName extends BaseEntity {
    private String tableName; /* 表名 */
    private String columnName; /* 列名 */
    private String referencedTableName; /* 外键对应的表 */
    private String referencedColumnName; /* 外键表的字段*/
}
