package com.os.common.entity.system;

import com.os.common.entity.base.BaseEntity;
import lombok.*;

/**
 * 描述：用户信息实体类
 *
 * @author huxuehao
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SysUser extends BaseEntity {
    private String id;
    private String userName;
    private String password;
    private Integer delFlag;
}
