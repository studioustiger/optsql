package com.os.common.entity;

import com.os.common.entity.base.BaseEntity;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Test extends BaseEntity {
    private Long id;
    private String name;
}
