package com.os.common.entity.field;

import com.os.common.entity.base.BaseEntity;
import lombok.*;

import java.util.List;

/**
 * 描述：字段删除实体类
 *
 * @author huxuehao
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FieldDelete extends BaseEntity {
    private List<String> tableNames;
    /* 字段名称 正则*/
    private String fieldNameRule;
    /* 字段类型 正则*/
    private String fieldTypeRule;
    /* 字段注释 正则*/
    private String fieldCommentRule;
    private String datasourceId;
}
