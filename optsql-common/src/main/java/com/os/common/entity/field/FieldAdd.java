package com.os.common.entity.field;

import com.os.common.entity.base.BaseEntity;
import lombok.*;

import java.util.List;

/**
 * 描述：添加字段的描述
 *
 * @author huxuehao
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FieldAdd extends BaseEntity {
    private List<String> tableNames; /* 表名称 */
    private String fieldName; /* 字段名称 */
    private String fieldType; /* 字段类型 */
    private Boolean fieldIsNull; /* 字段是否为空 */
    private String fieldComment; /* 字段备注 */
    private String characterSet; /* 字符集 */
    private String collate; /* 排序规则 */
    private Boolean isCover; /* 重复是否进行覆盖 */
    private String datasourceId;
}
