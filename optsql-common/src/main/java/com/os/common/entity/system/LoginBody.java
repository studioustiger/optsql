package com.os.common.entity.system;

import com.os.common.entity.base.BaseEntity;
import lombok.*;

/**
 * 描述：
 *
 * @author huxuehao
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LoginBody extends BaseEntity {
    private String userName;
    private String password;
}
