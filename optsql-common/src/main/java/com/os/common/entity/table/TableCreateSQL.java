package com.os.common.entity.table;

import com.os.common.entity.base.BaseEntity;
import lombok.*;

/**
 * 描述：接收表的创建语句实体
 *
 * @author huxuehao
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TableCreateSQL extends BaseEntity {
    private String tableName;
    private String createTableSQL;
}
