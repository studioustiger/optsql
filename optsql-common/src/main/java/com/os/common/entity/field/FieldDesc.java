package com.os.common.entity.field;

import com.os.common.entity.base.BaseEntity;
import lombok.*;

/**
 * 描述：字段描述实体类
 *
 * @author huxuehao
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FieldDesc extends BaseEntity {
    private String field; /* 字段名 */
    private String type; /* 类型*/
    private String isNull; /* 是否为空 YES */
    private String key; /* 是否是主键 PRI */
    private String comment; /* 注释 */
}
