package com.os.common.constant;

/**
 * @ClassName Constant
 * @Description 常量
 * @Author StudiousTiger
 **/
/**
 * 描述：用于方法，其作用是标记是否切换动态数据源
 *
 * @author huxuehao
 **/
public class Constant {
    /* token秘钥 */
    public static final String JWT_SECRET = "hudjkjsRe&fh*xuejakh@18908=hao";
    public static final String LOGIN_USER_KEY = "os-login-user-key";
    /* 存活时间*/
    public static final Long LIVE_TIME = 72000000L;
    /* 更新存活时间*/
    public static final Long REF_LIVE_TIME = 18000000L;

    /* 解析文件时使用的常量 */
    public static final String EXCEL = "excel";
    public static final String CSV = "csv";
    public static final String TXT = "txt";
    public static final String XLS_SUFFIX = ".xls";
    public static final String XLSX_SUFFIX = ".xlsx";
    public static final String CSV_SUFFIX = ".csv";
    public static final String TXT_SUFFIX = ".txt";
}
