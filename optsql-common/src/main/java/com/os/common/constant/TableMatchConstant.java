package com.os.common.constant;

/**
 * @ClassName TableMatchConstant
 * @Description TODO
 * @Author huxuehao
 **/
/**
 * 描述：用于方法，其作用是标记是否切换动态数据源
 *
 * @author huxuehao
 **/
public class TableMatchConstant {
    public static final String IGNORE = "ignore";
    public static final String ADD = "add";
    public static final String DELETE = "delete";
}
