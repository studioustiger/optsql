package com.os.common.constant;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @ClassName CommonProperties
 * @Description httpUtils参数
 * @Author huxuehao
 **/
/**
 * 描述：用于方法，其作用是标记是否切换动态数据源
 *
 * @author huxuehao
 **/
@Data
@Component
@ConfigurationProperties(prefix = "optsql.http-util", ignoreUnknownFields = false)
public class HttpProperties {
    private int socketTimeout = 60000;
    private int connectTimeout = 30000;
    private int connectionRequestTimeout = 60000;
    private int maxTotal = 1500;
    private int defaultMaxPerRoute = 300;
}
