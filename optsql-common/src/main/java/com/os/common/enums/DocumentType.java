package com.os.common.enums;

/**
 * 描述：数据库文档类型
 *
 * @author huxuehao
 **/
public enum DocumentType {
    WORD(100,"word文档"),
    HTML(101, "html文档"),
    MARKDOWN(102, "markdown文档");

    public int code;
    public String msg;

    DocumentType(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int code() {
        return this.code;
    }

    public String msg() {
        return this.msg;
    }
}
