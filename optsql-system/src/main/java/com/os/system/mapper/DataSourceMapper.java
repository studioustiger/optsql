package com.os.system.mapper;

import com.os.common.entity.datasource.DataSource;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.LinkedList;

/**
 * 描述: 取数据源信息
 * @author StudiousTiger
 **/
@Repository
public interface DataSourceMapper {
    /**
     * 获取数据源列表
     */
    LinkedList<DataSource> selectList();

    DataSource selectById(@Param("id") String id);

    /**
     * 新增
     * @param dataSource 数据源实体
     */
    int add(DataSource dataSource);

    /**
     * 删除
     *
     * @param ids IDS
     */
    int deleteByIds(@Param("ids") String[] ids);

    /**
     * 修改
     *
     * @param dataSource 数据源实体
     */
    int update(DataSource dataSource);


}
