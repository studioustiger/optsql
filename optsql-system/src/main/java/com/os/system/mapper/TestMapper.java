package com.os.system.mapper;

import com.os.common.entity.Test;
import org.springframework.stereotype.Repository;

import java.util.LinkedList;

/**
 * 描述:
 * @author StudiousTiger
 **/
@Repository
public interface TestMapper {
    LinkedList<Test> selectList();
}
