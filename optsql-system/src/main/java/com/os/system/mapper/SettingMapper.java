package com.os.system.mapper;

import com.os.common.entity.system.SysSetting;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.LinkedList;

/**
 * 描述:
 * @author StudiousTiger
 **/
@Repository
public interface SettingMapper {
    /**
     * 获取设置列表
     */
    LinkedList<SysSetting> selectList();

    /**
     * 根据id删除设置
     */
    int delSettingById(@Param("id")String id);

    /**
     * 更新设置
     */
    int updateSetting(SysSetting setting);
}
