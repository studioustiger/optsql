package com.os.system.service.impl;

import com.os.common.entity.datasource.DataSource;
import com.os.common.utils.MyUtil;
import com.os.system.mapper.DataSourceMapper;
import com.os.system.service.DataSourceService;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

/**
 * 描述: 数据源接口试下类
 * @author StudiousTiger
 **/
@Service
public class DataSourceServiceImpl implements DataSourceService {
    private final DataSourceMapper dataSourceMapper;

    public DataSourceServiceImpl(DataSourceMapper dataSourceMapper) {
        this.dataSourceMapper = dataSourceMapper;
    }

    @Override
    public List<DataSource> selectList() throws SQLException {
        try {
            return dataSourceMapper.selectList();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public DataSource selectById(String id) throws SQLException {
        try {
            return dataSourceMapper.selectById(id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public DataSource add(DataSource dataSource) throws SQLException {
        dataSource.setId(MyUtil.nextId());
        try {
            dataSourceMapper.add(dataSource);
            return dataSource;
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public int deleteByIds(String[] ids) throws SQLException {
        try {
            return dataSourceMapper.deleteByIds(ids);
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public int update(DataSource dataSource) throws SQLException {
        try {
            return dataSourceMapper.update(dataSource);
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException(e.getMessage());
        }
    }
}
