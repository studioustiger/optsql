package com.os.system.service.impl;

import com.os.common.entity.Test;
import com.os.system.mapper.TestMapper;
import com.os.system.service.TestService;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

/**
 * 描述: 测试
 * @author StudiousTiger
 **/
@Service
public class TestServiceImpl implements TestService {
    private final TestMapper testMapper;

    public TestServiceImpl(TestMapper testMapper) {
        this.testMapper = testMapper;
    }

    @Override
    public List<Test> selectList() throws SQLException {
        try {
            return  testMapper.selectList();
        } catch (Exception e) {
            throw new SQLException(e.getMessage());
        }
    }
}
