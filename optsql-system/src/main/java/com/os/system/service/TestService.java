package com.os.system.service;

import com.os.common.entity.Test;

import java.sql.SQLException;
import java.util.List;

/**
 * 描述: 测试接口
 * @author StudiousTiger
 **/
public interface TestService {

    List<Test> selectList() throws SQLException;
}
