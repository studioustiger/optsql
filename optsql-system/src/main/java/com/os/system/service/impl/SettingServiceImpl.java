package com.os.system.service.impl;

import com.os.common.entity.system.SysSetting;
import com.os.system.mapper.SettingMapper;
import com.os.system.service.SettingService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 描述: 设置接口实现类
 * @author StudiousTiger
 **/
@Service
public class SettingServiceImpl implements SettingService {
    private final SettingMapper settingMapper;

    public SettingServiceImpl(SettingMapper settingMapper) {
        this.settingMapper = settingMapper;
    }

    @Override
    public List<SysSetting> selectList() {
        return settingMapper.selectList();
    }

    @Override
    public int delSettingById(String id) {
        return settingMapper.delSettingById(id);
    }

    @Override
    public int updateSetting(SysSetting setting) {
        return settingMapper.updateSetting(setting);
    }
}
