package com.os.system.service;

import com.os.common.entity.system.SysSetting;

import java.util.List;

/**
 * 描述: 设置接口
 * @author StudiousTiger
 **/
public interface SettingService {
    /**
     * 获取设置列表
     */
    List<SysSetting> selectList();

    /**
     * 根据id删除设置
     */
    int delSettingById(String id);

    /**
     * 更新设置
     */
    int updateSetting(SysSetting setting);
}
