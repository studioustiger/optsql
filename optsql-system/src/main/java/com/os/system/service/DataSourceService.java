package com.os.system.service;

import com.os.common.entity.datasource.DataSource;

import java.sql.SQLException;
import java.util.List;

/**
 * 描述: 数据源接口
 * @author StudiousTiger
 **/
public interface DataSourceService {
    List<DataSource> selectList() throws SQLException;

    DataSource selectById(String id) throws SQLException;

    DataSource add(DataSource dataSource) throws SQLException;

    int deleteByIds(String[] ids) throws SQLException;

    int update(DataSource dataSource) throws SQLException;
}
