package com.os.controller.base;

import com.os.common.annotation.auth.LoginAuth;
import com.os.common.response.Response;
import com.os.core.service.base.BaseInfoService;
import org.springframework.context.annotation.Description;
import org.springframework.web.bind.annotation.*;

/**
 * 描述：获取数据库表的详情信息
 *
 * @author huxuehao
 **/
@RestController
@RequestMapping(value = "/base/info")
public class InfoController {
    private final BaseInfoService baseInfoService;

    public InfoController(BaseInfoService baseInfoService) {
        this.baseInfoService = baseInfoService;
    }

    @LoginAuth
    @Description(value = "获取表详情列表")
    @GetMapping(value = "/select")
    public Response selectList(
            @RequestParam("datasourceId") String datasourceId,
            @RequestParam(value = "tableName",defaultValue = "") String tableName,
            @RequestParam(value = "tableType",defaultValue = "") String tableType) throws Exception {
        return Response.success(baseInfoService.selectList(datasourceId, tableName,tableType));
    }

    @LoginAuth
    @Description(value = "获取表的列详情")
    @GetMapping(value = "/column-details")
    public Response selectColumnDetails(
            @RequestParam("datasourceId") String datasourceId,
            @RequestParam(value = "tableName",defaultValue = "") String tableName) throws Exception {
        return Response.success(baseInfoService.selectColumnDetails(datasourceId, tableName));
    }

    @LoginAuth
    @Description(value = "获取表的DDL语句")
    @GetMapping(value = "/ddl")
    public Response getDDL(
            @RequestParam("datasourceId") String datasourceId,
            @RequestParam("tableName") String tableName,
            @RequestParam("tableType") String tableType) throws Exception {
        return Response.success(baseInfoService.getCreateSql(datasourceId, tableName, tableType));
    }
}
