package com.os.controller.dataopt;

import com.os.common.annotation.auth.LoginAuth;
import com.os.common.entity.extend.ParentChildMatrix;
import com.os.common.entity.field.FieldMerge;
import com.os.common.entity.field.FieldNameAlter;
import com.os.common.response.Response;
import com.os.common.utils.MyUtil;
import com.os.core.service.dataopt.DataoptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.web.bind.annotation.*;

import javax.naming.NotContextException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 描述：对表数据的操作
 *
 * @author huxuehao
 **/
@RestController
@RequestMapping("/dataopt")
public class DataOptController {
    private final DataoptService dataoptService;

    public DataOptController(DataoptService dataoptService) {
        this.dataoptService = dataoptService;
    }

    @LoginAuth
    @Description(value = "简单删除表数据")
    @PostMapping(value = "/delete/table-data/simple")
    public Response deleteTableDataOfSimple(@RequestBody Object o) {
        Map<String,Object> map = MyUtil.ObjectToMap(o);
        List<String> list = dataoptService.deleteTableDataOfSimple((List<String>) map.get("tableNames"), (String) map.get("filterSql"));
        return Response.success(list.stream().collect(Collectors.joining("\n\n")));
    }

    @LoginAuth
    @Description(value = "删除主键及其外键数据")
    @PostMapping(value = "/delete/table-data/primary")
    public Response deleteTableDataOfPrimary(@RequestBody Object o) {
        Map<String,Object> map = MyUtil.ObjectToMap(o);
        List<String> list = dataoptService.deleteTableDataOfPrimary((String) map.get("datasourceId"), (List<String>) map.get("tableNames"), (String) map.get("filterSql"));
        return Response.success(list.stream().collect(Collectors.joining("\n\n")));
    }

    @LoginAuth
    @Description(value = "给指定字段添加前缀或后缀")
    @PostMapping(value = "/alert/field-value")
    public Response alertFieldValue(@RequestBody FieldNameAlter fieldNameAlter) {
        return Response.success(dataoptService.alertFieldValue(fieldNameAlter));
    }

    @LoginAuth
    @Description(value = "合并字段")
    @PostMapping(value = "/merge/fields")
    public Response mergeFields(@RequestBody FieldMerge fieldMerge) {
        return Response.success(dataoptService.mergeFields(fieldMerge));
    }

    @LoginAuth
    @Description(value = "获取父子矩阵")
    @PostMapping(value = "/matrix/parent-child")
    public Response parentChildMatrix(@RequestBody ParentChildMatrix matrix) {
        return Response.success(dataoptService.parentChildMatrix(matrix));
    }

    @LoginAuth
    @Description(value = "获取表的逻辑外键")
    @GetMapping(value = "/foreignKey/of/primaryKey")
    public Response foreignKeyOfPrimaryKey(@RequestParam("datasourceId") String datasourceId, @RequestParam("tableName") String tableName) throws NotContextException {
        return Response.success(dataoptService.foreignKeyOfPrimaryKey(datasourceId, tableName));
    }
}
