package com.os.controller.extend;

import com.os.common.annotation.auth.LoginAuth;
import com.os.common.entity.extend.DocumentInfo;
import com.os.core.service.extend.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

/**
 * 描述：数据库设计文档生成
 *
 * @author huxuehao
 **/
@RestController
@RequestMapping(value = "/extend/doc")
public class DocumentController {
    private final DocumentService documentService;

    public DocumentController(DocumentService documentService) {
        this.documentService = documentService;
    }


    @LoginAuth
    @Description("生成word数据库文档")
    @PostMapping(value = "/word")
    public void genWord(@RequestBody DocumentInfo documentInfo, HttpServletResponse response) throws SQLException {
        documentService.genWord(documentInfo, response);
    }

    @LoginAuth
    @Description("生成word数据库文档")
    @PostMapping(value = "/html")
    public void genHtml(@RequestBody DocumentInfo documentInfo, HttpServletResponse response) throws SQLException {
        documentService.genHtml(documentInfo, response);
    }

    @LoginAuth
    @Description("生成word数据库文档")
    @PostMapping(value = "/md")
    public void genDM(@RequestBody DocumentInfo documentInfo, HttpServletResponse response) throws SQLException {
        documentService.genMD(documentInfo, response);
    }
}
