package com.os.controller.extend;

import com.os.common.annotation.auth.LoginAuth;
import com.os.common.entity.extend.TableMatch;
import com.os.common.response.Response;
import com.os.core.service.extend.TableMatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * 描述：表结构对比
 *
 * @author huxuehao
 **/
@RestController
@RequestMapping(value = "/extend/table")
public class TableMatchController {
    private final TableMatchService tableMatchService;

    public TableMatchController(TableMatchService tableMatchService) {
        this.tableMatchService = tableMatchService;
    }

    @LoginAuth
    @Description("表结构对比-生成脚本")
    @PostMapping(value = "/match/script")
    public Response matchReturnScript(@RequestBody TableMatch tableMatch) throws Exception {
        return Response.success(tableMatchService.matchReturnScript(tableMatch));
    }

    @LoginAuth
    @Description("表结构对比-下载文件")
    @PostMapping(value = "/match/file")
    public void matchReturnFile(@RequestBody TableMatch tableMatch, HttpServletResponse response) throws Exception {
        tableMatchService.matchReturnFile(tableMatch, response);
    }
}
