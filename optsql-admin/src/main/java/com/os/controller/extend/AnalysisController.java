package com.os.controller.extend;

import com.os.common.annotation.auth.LoginAuth;
import com.os.common.constant.Constant;
import com.os.common.exception.ErrorException;
import com.os.common.response.Response;
import com.os.core.service.extend.AnalysisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * 描述：文件解析
 *
 * @author huxuehao
 **/
@RestController
@RequestMapping(value = "/extend/file")
public class AnalysisController {

    private final AnalysisService analysisService;

    public AnalysisController(AnalysisService analysisService) {
        this.analysisService = analysisService;
    }

    @LoginAuth
    @Description("解析文件，获取导入脚本")
    @PostMapping("/analysis/file")
    public Response analysisFile(@RequestParam(value = "file") MultipartFile file) throws Exception {
        String fileType = getFileType(file);
        if (Constant.EXCEL.equalsIgnoreCase(fileType)) {
            return Response.success(analysisService.analysisExcel(file));
        } else if (Constant.CSV.equalsIgnoreCase(fileType)) {
            return Response.success(analysisService.analysisCsv(file));
        } else {
            return Response.success(analysisService.analysisTxt(file));
        }
    }

    @LoginAuth
    @Description("解析excel，获取导入脚本")
    @PostMapping("/analysis/excel")
    public Response analysisExcel(@RequestParam(value = "file") MultipartFile excelFile) throws Exception {
        return Response.success(analysisService.analysisExcel(excelFile));
    }

    @LoginAuth
    @Description("解析scv，获取导入脚本")
    @PostMapping("/analysis/scv")
    public Response analysisCsv(@RequestParam(value = "file") MultipartFile csvFile) throws Exception {
        return Response.success(analysisService.analysisCsv(csvFile));
    }

    @LoginAuth
    @Description("解析excel，获取导入脚本")
    @PostMapping("/analysis/txt")
    public Response analysisTxt(@RequestParam(value = "file") MultipartFile txtFile) throws Exception {
        return Response.success(analysisService.analysisTxt(txtFile));
    }

    @LoginAuth
    @Description("解析文件，下载导入脚本")
    @PostMapping("/analysis/file/download")
    public void analysisFileAndDownload(@RequestParam(value = "file") MultipartFile file, HttpServletResponse response) throws Exception {
        String fileType = getFileType(file);
        if (Constant.EXCEL.equalsIgnoreCase(fileType)) {
            analysisService.analysisExcelAndDownLoad(file, response);
        } else if (Constant.CSV.equalsIgnoreCase(fileType)) {
            analysisService.analysisCsvAndDownLoad(file, response);
        } else {
            analysisService.analysisTxtAndDownLoad(file, response);
        }
    }

    @LoginAuth
    @Description("解析excel文件，下载导入脚本")
    @PostMapping("/analysis/excel/download")
    public void analysisExcelAndDownload(@RequestParam(value = "file") MultipartFile file, HttpServletResponse response) throws Exception {
        analysisService.analysisExcelAndDownLoad(file, response);
    }

    @LoginAuth
    @Description("解析csv文件，下载导入脚本")
    @PostMapping("/analysis/csv/download")
    public void analysisCsvAndDownload(@RequestParam(value = "file") MultipartFile file, HttpServletResponse response) throws Exception {
        analysisService.analysisCsvAndDownLoad(file, response);
    }

    @LoginAuth
    @Description("解析txt文件，下载导入脚本")
    @PostMapping("/analysis/txt/download")
    public void analysisTxtAndDownload(@RequestParam(value = "file") MultipartFile file, HttpServletResponse response) throws Exception {
        analysisService.analysisTxtAndDownLoad(file, response);
    }

    /* 获取文件类型 */
    private String getFileType(MultipartFile file){
        String filename = file.getOriginalFilename();
        if (filename.endsWith(Constant.XLS_SUFFIX) || filename.endsWith(Constant.XLSX_SUFFIX)) {
            return Constant.EXCEL;
        } else if (filename.endsWith(Constant.CSV_SUFFIX)) {
            return Constant.CSV;
        } else if (filename.endsWith(Constant.TXT_SUFFIX)) {
            return Constant.TXT;
        }
        throw new ErrorException("抱歉，文件类型不匹配");
    }
}
