package com.os.controller;

import com.os.common.annotation.auth.PassAuth;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 描述：静态资源处理
 *
 * @author huxuehao
 **/
@Controller
public class StaticController {
    @PassAuth
    @GetMapping({"","/", "/index","/index.html"})
    public String redirectIndex() {
        return "redirect:/op-sql/index.html";
    }
}
