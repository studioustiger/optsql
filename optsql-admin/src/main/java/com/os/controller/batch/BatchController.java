package com.os.controller.batch;

import com.os.common.annotation.auth.LoginAuth;
import com.os.common.entity.table.TableNameAlter;
import com.os.common.response.Response;
import com.os.core.service.batch.BatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 描述：对表的批量操作
 *
 * @author huxuehao
 **/
@RestController
@RequestMapping(value = "/batch")
public class BatchController {
    private final BatchService batchService;

    public BatchController(BatchService batchService) {
        this.batchService = batchService;
    }

    @LoginAuth
    @Description(value = "批量修改表名")
    @PostMapping(value = "/alter/tableName/sql")
    public Response batchAlterTableName(@RequestBody TableNameAlter tableNameAlter) throws Exception {
        return Response.success(batchService.AlterTableName(tableNameAlter.getDatasourceId(), tableNameAlter));
    }
}
