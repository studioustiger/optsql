package com.os.controller.system;

import com.alibaba.fastjson2.JSON;
import com.os.common.annotation.auth.LoginAuth;
import com.os.common.entity.system.SysSetting;
import com.os.common.response.Response;
import com.os.system.service.SettingService;
import com.os.system.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 描述：系统设置操作
 *
 * @author huxuehao
 **/
@RestController
@RequestMapping(value = "/setting")
public class SysSettingController {
    private final UserService userService;
    private final SettingService settingService;

    public SysSettingController(UserService userService, SettingService settingService) {
        this.userService = userService;
        this.settingService = settingService;
    }

    @LoginAuth
    @Description(value = "修改密码")
    @PostMapping(value = "/change/password")
    public Response login(@RequestBody Object object) {
        Map<String, String> map = JSON.parseObject(JSON.toJSONString(object), Map.class);
        if ( userService.updatePassword(map) > 0) {
            return Response.success("密码修改成功");
        }
        return Response.error("密码修改失败");
    }

    @LoginAuth
    @Description(value = "获取设置列表")
    @GetMapping(value = "/select/setting")
    public Response selectSetting() {
        return Response.success(settingService.selectList());
    }

    @LoginAuth
    @Description(value = "修改设置状态")
    @PostMapping(value = "/change/setting")
    public Response updateSetting(@RequestBody SysSetting setting) {
        return Response.success(settingService.updateSetting(setting));
    }
}
