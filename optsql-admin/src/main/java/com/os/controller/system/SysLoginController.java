package com.os.controller.system;

import com.os.common.annotation.auth.LoginAuth;
import com.os.common.annotation.auth.PassAuth;
import com.os.common.entity.system.LoginBody;
import com.os.common.response.Response;
import com.os.system.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.web.bind.annotation.*;

/**
 * 描述：登录操作
 *
 * @author huxuehao
 **/
@RestController
public class SysLoginController {
    private final UserService userService;

    public SysLoginController(UserService userService) {
        this.userService = userService;
    }

    @PassAuth
    @Description(value = "登录")
    @PostMapping(value = "/login")
    public Response login(@RequestBody LoginBody loginBody) {
        return Response.success(userService.validateLogin(loginBody));
    }

    @LoginAuth
    @Description(value = "退出登录")
    @GetMapping(value = "/logout")
    public Response logout(@RequestParam("userId") String userId) {
        userService.logout(userId);
        return Response.success("退出成功");
    }
}
