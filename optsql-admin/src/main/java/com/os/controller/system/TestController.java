package com.os.controller.system;

import com.os.common.annotation.auth.LoginAuth;
import com.os.common.annotation.auth.PassAuth;
import com.os.common.entity.Test;
import com.os.common.response.Response;
import com.os.core.datasource.DBChangeService;
import com.os.system.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 描述：测试
 *
 * @author huxuehao
 **/
@RestController
@RequestMapping("/test")
public class TestController {
    private final TestService testService;
    private final DBChangeService dbChangeService;

    public TestController(TestService testService, DBChangeService dbChangeService) {
        this.testService = testService;
        this.dbChangeService = dbChangeService;
    }

    @LoginAuth
    @PostMapping(value = "/loginAuth",name = "鉴权测试")
    public Response loginAuth() {
        return Response.success("OK","鉴权测试成功");
    }

    @PassAuth
    @PostMapping(value = "/passAuth",name = "放权测试")
    public Response passAuth() {
        return Response.success("OK","放权测试成功");
    }

    @PassAuth
    @GetMapping(value = "/dbChange")
    public Response dbChange(@RequestParam("datasourceId") String datasourceId) throws Exception {
        boolean res= dbChangeService.changeBD(datasourceId);
        if (res) {
            List<Test> tests = testService.selectList();
            return Response.success("操作成功", tests);
        } else {
            return Response.success("数据源切换失败");
        }
    }
}
