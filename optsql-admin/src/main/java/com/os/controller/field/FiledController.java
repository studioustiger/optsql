package com.os.controller.field;

import com.os.common.annotation.auth.LoginAuth;
import com.os.common.entity.field.FieldAdd;
import com.os.common.entity.field.FieldAlter;
import com.os.common.entity.field.FieldDelete;
import com.os.common.response.Response;
import com.os.core.service.field.FieldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 描述：表字段操作
 *
 * @author huxuehao
 **/
@RestController
@RequestMapping(value = "/filed/batch")
public class FiledController {
    private final FieldService fieldService;

    public FiledController(FieldService fieldService) {
        this.fieldService = fieldService;
    }

    @LoginAuth
    @Description(value = "添加表字段")
    @PostMapping(value = "/add")
    public Response addField(@RequestBody FieldAdd fieldAdd) {
        List<String> list = fieldService.filedBatchAdd(fieldAdd.getDatasourceId(), fieldAdd);
        return Response.success(list);
    }

    @LoginAuth
    @Description(value = "删除表字段")
    @PostMapping(value = "/delete")
    public Response deleteField(@RequestBody FieldDelete fieldDelete) {
        List<String> list = fieldService.filedBatchDelete(fieldDelete.getDatasourceId(),fieldDelete);
        return Response.success(list);
    }

    @LoginAuth
    @Description(value = "修改表字段")
    @PostMapping(value = "/alter")
    public Response alterField(@RequestBody FieldAlter fieldAlter) {
        List<String> list = fieldService.filedBatchAlter(fieldAlter.getDatasourceId(), fieldAlter);
        return Response.success(list);
    }
}
